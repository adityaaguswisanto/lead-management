import 'package:lead_management/packages/packages.dart';

class VehicleItem extends StatelessWidget {
  final DataVehicle dataVehicle;
  final String category;
  final int idSelected;

  const VehicleItem({
    super.key,
    required this.dataVehicle,
    required this.category,
    required this.idSelected,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => CustomNavigation.intentWithData(
        context,
        MachinePage.routeName,
        MachineArgument(
          noPol: dataVehicle.noPol,
          category: category,
          id: idSelected,
        ),
      ),
      child: Container(
        width: CustomSize.width(context),
        margin: const EdgeInsets.only(
          bottom: 16,
        ),
        padding: const EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 16,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            8,
          ),
          border: Border.all(
            color: CustomColorStyle.black.withOpacity(
              0.2,
            ),
          ),
        ),
        child: Row(
          children: [
            Image.asset(
              dataVehicle.url,
              width: 30.w,
              height: 30.h,
            ),
            SizedBox(
              width: 12.w,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RichText(
                    text: TextSpan(
                      children: [
                        TextSpan(
                          text:
                              "${dataVehicle.model} ${dataVehicle.manufacture}   ",
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                        TextSpan(
                          text: "#${dataVehicle.noPol.replaceAll(' ', '')}",
                          style: CustomTextStyle.medium(
                            12.sp,
                            color: CustomColorStyle.green,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Text(
                    dataVehicle.address,
                    style: CustomTextStyle.medium(
                      12.sp,
                      color: CustomColorStyle.black.withOpacity(
                        0.6,
                      ),
                    ),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
