import 'package:lead_management/packages/packages.dart';

class CustomInfo extends StatelessWidget {
  final double width;
  final double height;
  final Color borderColor;
  final Color backgroundColor;
  final Color iconColor;

  const CustomInfo({
    super.key,
    required this.width,
    required this.height,
    required this.borderColor,
    required this.backgroundColor,
    required this.iconColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          width: 8,
          color: borderColor,
        ),
        color: backgroundColor,
      ),
      child: Icon(
        Icons.error_outline,
        color: iconColor,
      ),
    );
  }
}
