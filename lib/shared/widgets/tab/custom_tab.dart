import 'package:lead_management/packages/packages.dart';

class CustomTab extends StatelessWidget {
  final String title;
  final GestureTapCallback? onTap;
  final bool? active;
  final bool? status;

  const CustomTab({
    super.key,
    required this.title,
    required this.onTap,
    required this.active,
    required this.status,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Text(
            title,
            style: CustomTextStyle.medium(
              14.sp,
              color: active == status
                  ? CustomColorStyle.green
                  : CustomColorStyle.black.withOpacity(
                      0.4,
                    ),
            ),
          ),
          Container(
            width: CustomSize.width(context) / 4,
            margin: const EdgeInsets.only(
              top: 8,
            ),
            height: 2,
            color: active == status ? CustomColorStyle.green : null,
          )
        ],
      ),
    );
  }
}
