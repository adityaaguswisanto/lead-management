import 'package:lead_management/packages/packages.dart';

class CustomHandling extends StatelessWidget {
  final GestureTapCallback? onTap;
  final bool? withError;

  const CustomHandling({
    super.key,
    required this.onTap,
    this.withError,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            withError == true
                ? "assets/images/handling/error.svg"
                : "assets/images/handling/empty.svg",
            width: 120.w,
            height: 120.h,
          ),
          SizedBox(
            height: 30.h,
          ),
          Text(
            "Sorry, No Data :(",
            style: CustomTextStyle.bold(
              14.sp,
            ),
          ),
          SizedBox(
            height: 4.h,
          ),
          Text(
            "Tap the button below to refresh",
            style: CustomTextStyle.medium(
              14.sp,
              color: CustomColorStyle.black.withOpacity(
                0.4,
              ),
            ),
          ),
          SizedBox(
            height: 16.h,
          ),
          GestureDetector(
            onTap: onTap,
            child: Text(
              "Tap for refresh",
              style: CustomTextStyle.medium(
                14.sp,
                color: CustomColorStyle.green,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
