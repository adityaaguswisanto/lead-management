import 'package:lead_management/packages/packages.dart';

class CustomButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final bool? withIcon;
  final bool? withOutline;
  final Color? backgroundColor;
  final String label;
  final Color? labelColor;
  final double? labelSize;
  final IconData? iconData;
  final Color? iconColor;
  final double? iconSize;

  const CustomButton({
    super.key,
    required this.onPressed,
    this.withIcon,
    required this.label,
    this.backgroundColor,
    this.withOutline,
    this.labelColor,
    this.labelSize,
    this.iconData,
    this.iconColor,
    this.iconSize,
  });

  @override
  Widget build(BuildContext context) {
    return withIcon == true
        ? ElevatedButton.icon(
            onPressed: onPressed,
            icon: Icon(
              iconData,
              color: iconColor ?? CustomColorStyle.white,
              size: iconSize ?? 20,
            ),
            label: Text(
              label,
              style: CustomTextStyle.medium(
                labelSize ?? 12.sp,
                color: labelColor ?? CustomColorStyle.white,
              ),
            ),
            style: ElevatedButton.styleFrom(
              backgroundColor: backgroundColor ?? CustomColorStyle.green,
              elevation: 0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(
                  8,
                ),
              ),
            ),
          )
        : withOutline == true
            ? OutlinedButton(
                onPressed: onPressed,
                style: OutlinedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                      8,
                    ),
                  ),
                  side: BorderSide(
                    color: CustomColorStyle.black.withOpacity(
                      0.2,
                    ),
                  ),
                ),
                child: Text(
                  label,
                  style: CustomTextStyle.medium(
                    labelSize ?? 12.sp,
                    color: labelColor ?? CustomColorStyle.black,
                  ),
                ),
              )
            : ElevatedButton(
                onPressed: onPressed,
                style: ElevatedButton.styleFrom(
                  backgroundColor: backgroundColor ?? CustomColorStyle.green,
                  elevation: 0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(
                      8,
                    ),
                  ),
                ),
                child: Text(
                  label,
                  style: CustomTextStyle.medium(
                    labelSize ?? 12.sp,
                    color: labelColor ?? CustomColorStyle.white,
                  ),
                ),
              );
  }
}
