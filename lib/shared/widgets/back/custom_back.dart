import 'package:lead_management/packages/packages.dart';

class CustomBack extends StatelessWidget {
  final String title;
  final PopInvokedCallback? onPopInvoked;
  final GestureTapCallback onTap;

  const CustomBack({
    super.key,
    required this.title,
    required this.onPopInvoked,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      onPopInvoked: onPopInvoked,
      child: GestureDetector(
        onTap: onTap,
        child: Row(
          children: [
            Icon(
              Icons.arrow_back,
              color: CustomColorStyle.green,
              size: 20,
            ),
            SizedBox(
              width: 8.w,
            ),
            Text(
              title,
              style: CustomTextStyle.medium(
                12.sp,
                color: CustomColorStyle.green,
              ),
            )
          ],
        ),
      ),
    );
  }
}
