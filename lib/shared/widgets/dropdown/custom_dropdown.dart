import 'package:lead_management/packages/packages.dart';

class CustomDropdown<T> extends StatelessWidget {
  final String label;
  final T? value;
  final List<DropdownMenuItem<T>>? items;
  final ValueChanged<T?>? onChanged;
  final bool isExpanded;
  final bool withFlag;

  const CustomDropdown({
    super.key,
    required this.label,
    required this.value,
    required this.items,
    required this.onChanged,
    this.isExpanded = false,
    this.withFlag = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: CustomSize.width(context),
      margin: const EdgeInsets.only(
        top: 8,
        bottom: 16,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 16,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(
          8,
        ),
        border: Border.all(
          color: CustomColorStyle.black.withOpacity(
            0.2,
          ),
        ),
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<T>(
          isExpanded: isExpanded,
          hint: withFlag == false
              ? Text(
                  label,
                  style: CustomTextStyle.medium(
                    12.sp,
                    color: CustomColorStyle.black.withOpacity(
                      0.4,
                    ),
                  ),
                )
              : Row(
                  children: [
                    Image.asset(
                      "assets/images/machine/flag.png",
                    ),
                    SizedBox(
                      width: 8.w,
                    ),
                    Text(
                      label,
                      style: CustomTextStyle.medium(
                        12.sp,
                        color: CustomColorStyle.black.withOpacity(
                          0.4,
                        ),
                      ),
                    ),
                  ],
                ),
          icon: const Icon(
            Icons.expand_more_rounded,
          ),
          value: value,
          items: items,
          onChanged: onChanged,
        ),
      ),
    );
  }
}
