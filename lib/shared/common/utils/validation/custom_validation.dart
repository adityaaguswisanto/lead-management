class CustomValidation {
  static empty(String field, String value) {
    if (value.isEmpty) {
      return "$field is not empty";
    }
    return null;
  }
}
