import 'package:lead_management/packages/packages.dart';

class CustomNavigation {
  static back(BuildContext context) {
    Navigator.pop(
      context,
    );
  }

  static intent(
    BuildContext context,
    String nameRouted, {
    Object? arguments,
  }) {
    Navigator.pushNamed(
      context,
      nameRouted,
      arguments: arguments,
    );
  }

  static intentWithoutBack(
    BuildContext context,
    String nameRouted, {
    Object? arguments,
  }) {
    Navigator.pushReplacementNamed(
      context,
      nameRouted,
      arguments: arguments,
    );
  }

  static intentWithClearAllRoutes(
    BuildContext context,
    String nameRouted, {
    Object? arguments,
  }) {
    Navigator.of(context).pushNamedAndRemoveUntil(
      nameRouted,
      (route) => false,
      arguments: arguments,
    );
  }

  static intentWithData(
    BuildContext context,
    String nameRouted,
    Object argumentClass,
  ) {
    Navigator.pushNamed(
      context,
      nameRouted,
      arguments: argumentClass,
    );
  }
}
