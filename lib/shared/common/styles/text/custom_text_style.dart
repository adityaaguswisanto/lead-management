import 'package:lead_management/packages/packages.dart';

class CustomTextStyle {
  static TextStyle regular(double fontSize, {Color? color}) => TextStyle(
        color: color ?? CustomColorStyle.black,
        fontFamily: "InterRegular",
        fontSize: fontSize,
      );

  static TextStyle medium(double fontSize, {Color? color}) => TextStyle(
        color: color ?? CustomColorStyle.black,
        fontFamily: "InterMedium",
        fontSize: fontSize,
      );

  static TextStyle bold(double fontSize, {Color? color}) => TextStyle(
        color: color ?? CustomColorStyle.black,
        fontFamily: "InterBold",
        fontSize: fontSize,
      );
}
