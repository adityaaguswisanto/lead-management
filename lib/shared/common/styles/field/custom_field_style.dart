import 'package:lead_management/packages/packages.dart';

class CustomFieldStyle {
  static InputDecoration regular(
    String hintText, {
    IconData? icon,
    double? sizeIcon,
    Color? colorIcon,
    double? vertical,
    double? horizontal,
  }) =>
      InputDecoration(
        hintText: hintText,
        hintStyle: CustomTextStyle.medium(
          12.sp,
          color: CustomColorStyle.black.withOpacity(
            0.4,
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(
            width: 1,
            color: CustomColorStyle.red,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide(
            width: 1,
            color: CustomColorStyle.black.withOpacity(
              0.2,
            ),
          ),
        ),
        contentPadding: EdgeInsets.symmetric(
          vertical: vertical ?? 16,
          horizontal: horizontal ?? 16,
        ),
      );

  static InputDecoration regularWithoutBorder(
    String hintText, {
    IconData? icon,
    double? sizeIcon,
    Color? colorIcon,
  }) =>
      InputDecoration(
        hintText: hintText,
        border: InputBorder.none,
        hintStyle: CustomTextStyle.medium(
          12.sp,
          color: CustomColorStyle.black.withOpacity(
            0.4,
          ),
        ),
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 16,
        ),
      );
}
