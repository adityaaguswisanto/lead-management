import 'package:lead_management/packages/packages.dart';

class CustomColorStyle {
  static Color baseLayout = const Color(0xFFFCFCFC);
  static Color transparent = const Color(0x00ffffff);

  static Color white = const Color(0xffffffff);
  static Color black = const Color(0xff3b3b3b);
  static Color red = const Color(0xFFD92D20);
  static Color gray = const Color(0xFFF1F1F1);

  static Color fontBlack = const Color(0xFF101828);

  static Color green = const Color(0xFF24A896);
  static Color yellow = const Color(0xFFFFEAAA);
  static Color yellowDark = const Color(0xFFFFBF00);
  static Color yellowWhite = const Color(0xFFFFFAEB);
}
