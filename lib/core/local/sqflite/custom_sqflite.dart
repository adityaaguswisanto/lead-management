import 'package:lead_management/packages/packages.dart';

import 'package:path/path.dart';

class CustomSqflite {
  static final CustomSqflite instance = CustomSqflite._init();

  static Database? _database;

  CustomSqflite._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('lead_management.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 1, onCreate: createDatabase);
  }

  Future createDatabase(Database db, int version) async {
    const id = 'INTEGER PRIMARY KEY AUTOINCREMENT';
    const noPol = "TEXT";
    const condition = "TEXT";
    const brand = "TEXT";
    const url = "TEXT";
    const model = "TEXT";
    const variant = "TEXT";
    const manufacture = "TEXT";
    const mileage = "TEXT";
    const fuelType = "TEXT";
    const transmission = "TEXT";
    const exterior = "TEXT";
    const price = "TEXT";
    const notes = "TEXT";
    const seller = "TEXT";
    const address = "TEXT";
    const mobileNumber = "TEXT";
    const province = "TEXT";
    const district = "TEXT";
    const subDistrict = "TEXT";
    const status = "TEXT";
    const category = "TEXT";
    const createdAt = "INTEGER";
    const updatedAt = "INTEGER";

    await db.execute(
      'CREATE TABLE $vehicleTable (id $id, no_pol $noPol, condition $condition, brand $brand, url $url, model $model, variant $variant, manufacture $manufacture, mileage $mileage, fuel_type $fuelType, transmission $transmission, exterior $exterior, price $price, notes $notes, seller $seller, address $address, mobile_number $mobileNumber, province $province, district $district, sub_district $subDistrict, status $status, category $category, created_at $createdAt, updated_at $updatedAt)',
    );
  }

  Future<DataVehicleModel> readMachine(String noPol) async {
    final db = await instance.database;
    final results = await db.query(
      vehicleTable,
      where: "no_pol = ?",
      whereArgs: [noPol],
    );
    return results.map((e) => DataVehicleModel.fromJson(e)).first;
  }

  Future<VehicleModel> readVehicle(
    String status,
    String category,
    int offset,
    int limit,
  ) async {
    final db = await instance.database;
    final results = await db.query(
      vehicleTable,
      where: "status = ? and category = ?",
      whereArgs: [status, category],
      orderBy: "updated_at DESC",
      offset: offset,
      limit: limit,
    );
    return VehicleModel(
      dataVehicle: results.map((e) => DataVehicleModel.fromJson(e)).toList(),
    );
  }

  Future<DataVehicleModel> createVehicle(
    DataVehicleModel dataVehicleModel,
  ) async {
    final db = await instance.database;
    await db.insert(
      vehicleTable,
      dataVehicleModel.toJson(),
    );
    return dataVehicleModel;
  }

  Future<DataVehicleModel> updateMachine(
    DataVehicleModel dataVehicleModel,
  ) async {
    final db = await instance.database;
    await db.update(
      vehicleTable,
      dataVehicleModel.toJson(),
      where: "no_pol = ?",
      whereArgs: [dataVehicleModel.noPol],
    );
    return dataVehicleModel;
  }

  Future<VehicleModel> readCollection(String noPol) async {
    final db = await instance.database;

    final results = await db.query(
      vehicleTable,
      where: "no_pol LIKE ?",
      orderBy: "updated_at DESC",
      whereArgs: ["%$noPol%"],
    );

    return VehicleModel(
      dataVehicle: results.map((e) => DataVehicleModel.fromJson(e)).toList(),
    );
  }

  Future<FinancingModel> readFinancing(
    String category,
  ) async {
    final db = await instance.database;

    late int listing;
    late int inspecting;
    late int visited;
    late int assigningSurveyor;
    late int surveying;
    late int approval;
    late int purchasingOrder;
    late int rejected;
    late int unitNotAvailable;

    final dataListing = await db.query(
      vehicleTable,
      where: "status = ? and category = ?",
      whereArgs: ["1", category],
    );

    listing = dataListing.length;

    final dataInspecting = await db.query(
      vehicleTable,
      where: "status = ? and category = ?",
      whereArgs: ["2", category],
    );

    inspecting = dataInspecting.length;

    final dataVisited = await db.query(
      vehicleTable,
      where: "status = ? and category = ?",
      whereArgs: ["3", category],
    );

    visited = dataVisited.length;

    final dataAssigningSurveyor = await db.query(
      vehicleTable,
      where: "status = ? and category = ?",
      whereArgs: ["4", category],
    );

    assigningSurveyor = dataAssigningSurveyor.length;

    final dataSurveying = await db.query(
      vehicleTable,
      where: "status = ? and category = ?",
      whereArgs: ["5", category],
    );

    surveying = dataSurveying.length;

    final dataApproval = await db.query(
      vehicleTable,
      where: "status = ? and category = ?",
      whereArgs: ["6", category],
    );

    approval = dataApproval.length;

    final dataPurchasingOrder = await db.query(
      vehicleTable,
      where: "status = ? and category = ?",
      whereArgs: ["7", category],
    );

    purchasingOrder = dataPurchasingOrder.length;

    final dataRejected = await db.query(
      vehicleTable,
      where: "status = ? and category = ?",
      whereArgs: ["8", category],
    );

    rejected = dataRejected.length;

    final dataUnitNotAvailable = await db.query(
      vehicleTable,
      where: "status = ? and category = ?",
      whereArgs: ["9", category],
    );

    unitNotAvailable = dataUnitNotAvailable.length;

    return FinancingModel(
      listing: listing,
      inspecting: inspecting,
      visited: visited,
      assigningSurveyor: assigningSurveyor,
      surveying: surveying,
      approval: approval,
      purchasingOrder: purchasingOrder,
      rejected: rejected,
      unitNotAvailable: unitNotAvailable,
    );
  }
}
