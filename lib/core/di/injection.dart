import 'package:lead_management/packages/packages.dart';

final locator = GetIt.instance;

void init() {
  // bloc
  initBloc();

  // usecase
  initUsecase();

  // repository
  initRepository();

  // data source
  initDataSource();

  // external
  initExternal();
}

Future<void> initBloc() async {
  locator.registerFactory(() => VehicleBloc(locator()));
  locator.registerFactory(() => FinancingBloc(locator()));
  locator.registerFactory(() => MachineBloc(locator()));
  locator.registerFactory(() => CollectionBloc(locator()));
}

Future<void> initUsecase() async {
  locator.registerLazySingleton(() => GetCurrentVehicle(locator()));
  locator.registerLazySingleton(() => GetCurrentFinancing(locator()));
  locator.registerLazySingleton(() => GetCurrentMachine(locator()));
  locator.registerLazySingleton(() => GetCurrentCollection(locator()));
}

Future<void> initRepository() async {
  locator.registerLazySingleton<VehicleRepository>(
      () => VehicleRepositoryImpl(vehicleSource: locator()));
  locator.registerLazySingleton<FinancingRepository>(
      () => FinancingRepositoryImpl(financingSource: locator()));
  locator.registerLazySingleton<MachineRepository>(
      () => MachineRepositoryImpl(machineSource: locator()));
  locator.registerLazySingleton<CollectionRepository>(
      () => CollectionRepositoryImpl(collectionSource: locator()));
}

Future<void> initDataSource() async {
  locator.registerLazySingleton<VehicleSource>(
      () => VehicleSourceImpl(customSqflite: locator()));
  locator.registerLazySingleton<FinancingSource>(
      () => FinancingSourceImpl(customSqflite: locator()));
  locator.registerLazySingleton<MachineSource>(
      () => MachineSourceImpl(customSqflite: locator()));
  locator.registerLazySingleton<CollectionSource>(
      () => CollectionSourceImpl(customSqflite: locator()));
}

Future<void> initExternal() async {
  locator.registerLazySingleton(
    () => CustomSqflite.instance,
  );
}
