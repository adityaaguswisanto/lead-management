import 'package:lead_management/packages/packages.dart';

abstract class MachineState extends Equatable {
  const MachineState();

  @override
  List<Object?> get props => [];
}

class MachineInitialized extends MachineState {}

class MachineLoading extends MachineState {}

class MachineGetSuccess extends MachineState {
  final DataVehicle dataVehicle;

  const MachineGetSuccess(this.dataVehicle);

  @override
  List<Object?> get props => [dataVehicle];
}

class MachinePutSuccess extends MachineState {
  final String message;

  const MachinePutSuccess(this.message);

  @override
  List<Object?> get props => [message];
}

class MachineFailure extends MachineState {
  final String message;

  const MachineFailure(this.message);

  @override
  List<Object?> get props => [message];
}
