import 'package:lead_management/packages/packages.dart';

abstract class MachineEvent extends Equatable {
  const MachineEvent();

  @override
  List<Object?> get props => [];
}

class MachineGetted extends MachineEvent {
  final String noPol;

  const MachineGetted({
    required this.noPol,
  });

  @override
  List<Object?> get props => [
        noPol,
      ];
}

class MachinePutted extends MachineEvent {
  final DataVehicleModel dataVehicleModel;

  const MachinePutted({
    required this.dataVehicleModel,
  });

  @override
  List<Object?> get props => [
        dataVehicleModel,
      ];
}
