import 'package:lead_management/packages/packages.dart';

class MachineBloc extends Bloc<MachineEvent, MachineState> {
  final GetCurrentMachine getCurrentMachine;

  MachineBloc(this.getCurrentMachine) : super(MachineInitialized()) {
    on<MachineGetted>(
      (event, emit) async {
        emit(MachineLoading());
        await Future.delayed(
          const Duration(
            seconds: 1,
          ),
        );
        final result = await getCurrentMachine.get(
          event.noPol,
        );
        result.fold((failure) {
          emit(MachineFailure(failure.message));
        }, (data) {
          emit(MachineGetSuccess(data));
        });
      },
    );

    on<MachinePutted>(
      (event, emit) async {
        emit(MachineLoading());
        final result = await getCurrentMachine.put(
          event.dataVehicleModel,
        );
        result.fold((failure) {
          emit(MachineFailure(failure.message));
        }, (data) {
          emit(const MachinePutSuccess("Success Updated"));
        });
      },
    );
  }
}
