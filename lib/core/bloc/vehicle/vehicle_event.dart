import 'package:lead_management/packages/packages.dart';

abstract class VehicleEvent extends Equatable {
  const VehicleEvent();

  @override
  List<Object?> get props => [];
}

class VehicleSubmitted extends VehicleEvent {
  final String noPol;
  final String condition;
  final String brand;
  final String url;
  final String model;
  final String variant;
  final String manufacture;
  final String mileage;
  final String fuelType;
  final String transmission;
  final String exterior;
  final String price;
  final String notes;
  final String seller;
  final String address;
  final String mobileNumber;
  final String province;
  final String district;
  final String subDistrict;
  final String status;
  final String category;
  final int createdAt;
  final int updatedAt;

  const VehicleSubmitted({
    required this.noPol,
    required this.condition,
    required this.brand,
    required this.url,
    required this.model,
    required this.variant,
    required this.manufacture,
    required this.mileage,
    required this.fuelType,
    required this.transmission,
    required this.exterior,
    required this.price,
    required this.notes,
    required this.seller,
    required this.address,
    required this.mobileNumber,
    required this.province,
    required this.district,
    required this.subDistrict,
    required this.status,
    required this.category,
    required this.createdAt,
    required this.updatedAt,
  });

  @override
  List<Object?> get props => [
        noPol,
        condition,
        brand,
        url,
        model,
        variant,
        manufacture,
        mileage,
        fuelType,
        transmission,
        exterior,
        price,
        notes,
        seller,
        address,
        mobileNumber,
        province,
        district,
        subDistrict,
        status,
        category,
        createdAt,
        updatedAt,
      ];
}

class VehicleGetted extends VehicleEvent {
  final String status;
  final String category;
  final int offset;
  final int limit;

  const VehicleGetted({
    required this.status,
    required this.category,
    required this.offset,
    required this.limit,
  });

  @override
  List<Object?> get props => [
        status,
        category,
        offset,
        limit,
      ];
}
