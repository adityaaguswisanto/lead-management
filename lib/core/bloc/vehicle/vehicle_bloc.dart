import 'package:lead_management/packages/packages.dart';

class VehicleBloc extends Bloc<VehicleEvent, VehicleState> {
  final GetCurrentVehicle getCurrentVehicle;

  VehicleBloc(this.getCurrentVehicle) : super(VehicleInitialized()) {
    on<VehicleSubmitted>(
      (event, emit) async {
        emit(VehicleLoading());
        await Future.delayed(
          const Duration(
            seconds: 1,
          ),
        );
        final result = await getCurrentVehicle.post(
          event.noPol,
          event.condition,
          event.brand,
          event.url,
          event.model,
          event.variant,
          event.manufacture,
          event.mileage,
          event.fuelType,
          event.transmission,
          event.exterior,
          event.price,
          event.notes,
          event.seller,
          event.address,
          event.mobileNumber,
          event.province,
          event.district,
          event.subDistrict,
          event.status,
          event.category,
          event.createdAt,
          event.updatedAt,
        );
        result.fold((failure) {
          emit(VehicleFailure(failure.message));
        }, (data) {
          emit(const VehiclePostSuccess("Success send to data"));
        });
      },
    );
    on<VehicleGetted>(
      (event, emit) async {
        emit(VehicleLoading());
        await Future.delayed(
          const Duration(
            seconds: 1,
          ),
        );
        final result = await getCurrentVehicle.get(
          event.status,
          event.category,
          event.offset,
          event.limit,
        );
        result.fold((failure) {
          emit(VehicleFailure(failure.message));
        }, (data) {
          emit(VehicleGetSuccess(data));
        });
      },
    );
  }
}
