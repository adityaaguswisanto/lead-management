import 'package:lead_management/packages/packages.dart';

abstract class VehicleState extends Equatable {
  const VehicleState();

  @override
  List<Object?> get props => [];
}

class VehicleInitialized extends VehicleState {}

class VehicleLoading extends VehicleState {}

class VehiclePostSuccess extends VehicleState {
  final String message;

  const VehiclePostSuccess(this.message);

  @override
  List<Object?> get props => [message];
}

class VehicleGetSuccess extends VehicleState {
  final Vehicle vehicle;

  const VehicleGetSuccess(this.vehicle);

  @override
  List<Object?> get props => [vehicle];
}

class VehicleFailure extends VehicleState {
  final String message;

  const VehicleFailure(this.message);

  @override
  List<Object?> get props => [message];
}
