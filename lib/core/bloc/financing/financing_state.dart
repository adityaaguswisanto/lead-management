import 'package:lead_management/packages/packages.dart';

abstract class FinancingState extends Equatable {
  const FinancingState();

  @override
  List<Object?> get props => [];
}

class FinancingInitialized extends FinancingState {}

class FinancingLoading extends FinancingState {}

class FinancingGetSuccess extends FinancingState {
  final Financing financing;

  const FinancingGetSuccess(this.financing);

  @override
  List<Object?> get props => [financing];
}

class FinancingFailure extends FinancingState {
  final String message;

  const FinancingFailure(this.message);

  @override
  List<Object?> get props => [message];
}
