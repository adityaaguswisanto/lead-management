import 'package:lead_management/packages/packages.dart';

abstract class FinancingEvent extends Equatable {
  const FinancingEvent();

  @override
  List<Object?> get props => [];
}

class FinancingGetted extends FinancingEvent {
  final String category;

  const FinancingGetted({
    required this.category,
  });

  @override
  List<Object?> get props => [
        category,
      ];
}
