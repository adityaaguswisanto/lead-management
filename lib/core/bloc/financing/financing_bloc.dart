import 'package:lead_management/packages/packages.dart';

class FinancingBloc extends Bloc<FinancingEvent, FinancingState> {
  final GetCurrentFinancing getCurrentFinancing;

  FinancingBloc(this.getCurrentFinancing) : super(FinancingInitialized()) {
    on<FinancingGetted>(
      (event, emit) async {
        emit(FinancingLoading());
        await Future.delayed(
          const Duration(
            seconds: 1,
          ),
        );
        final result = await getCurrentFinancing.get(
          event.category,
        );
        result.fold((failure) {
          emit(FinancingFailure(failure.message));
        }, (data) {
          emit(FinancingGetSuccess(data));
        });
      },
    );
  }
}
