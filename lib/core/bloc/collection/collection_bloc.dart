import 'package:lead_management/packages/packages.dart';

class CollectionBloc extends Bloc<CollectionEvent, CollectionState> {
  final GetCurrentCollection getCurrentCollection;

  CollectionBloc(this.getCurrentCollection) : super(CollectionInitialized()) {
    on<CollectionGetted>(
      (event, emit) async {
        emit(CollectionLoading());
        await Future.delayed(
          const Duration(
            seconds: 1,
          ),
        );
        final result = await getCurrentCollection.get(
          event.noPol,
        );
        result.fold((failure) {
          emit(CollectionFailure(failure.message));
        }, (data) {
          emit(CollectionGetSuccess(data));
        });
      },
    );
  }
}
