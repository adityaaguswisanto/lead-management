import 'package:lead_management/packages/packages.dart';

abstract class CollectionEvent extends Equatable {
  const CollectionEvent();

  @override
  List<Object?> get props => [];
}

class CollectionGetted extends CollectionEvent {
  final String noPol;

  const CollectionGetted({
    required this.noPol,
  });

  @override
  List<Object?> get props => [
        noPol,
      ];
}
