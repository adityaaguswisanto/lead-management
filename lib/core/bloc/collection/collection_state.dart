import 'package:lead_management/packages/packages.dart';

abstract class CollectionState extends Equatable {
  const CollectionState();

  @override
  List<Object?> get props => [];
}

class CollectionInitialized extends CollectionState {}

class CollectionLoading extends CollectionState {}

class CollectionGetSuccess extends CollectionState {
  final Vehicle vehicle;

  const CollectionGetSuccess(this.vehicle);

  @override
  List<Object?> get props => [vehicle];
}

class CollectionFailure extends CollectionState {
  final String message;

  const CollectionFailure(this.message);

  @override
  List<Object?> get props => [message];
}
