import 'package:lead_management/packages/packages.dart';

abstract class VehicleSource {
  Future<DataVehicleModel> postCurrentVehicle(
    String noPol,
    String condition,
    String brand,
    String url,
    String model,
    String variant,
    String manufacture,
    String mileage,
    String fuelType,
    String transmission,
    String exterior,
    String price,
    String notes,
    String seller,
    String address,
    String mobileNumber,
    String province,
    String district,
    String subDistrict,
    String status,
    String category,
    int createdAt,
    int updatedAt,
  );

  Future<VehicleModel> getCurrentVehicle(
    String status,
    String category,
    int offset,
    int limit,
  );
}

class VehicleSourceImpl implements VehicleSource {
  final CustomSqflite customSqflite;

  VehicleSourceImpl({
    required this.customSqflite,
  });

  @override
  Future<DataVehicleModel> postCurrentVehicle(
    String noPol,
    String condition,
    String brand,
    String url,
    String model,
    String variant,
    String manufacture,
    String mileage,
    String fuelType,
    String transmission,
    String exterior,
    String price,
    String notes,
    String seller,
    String address,
    String mobileNumber,
    String province,
    String district,
    String subDistrict,
    String status,
    String category,
    int createdAt,
    int updatedAt,
  ) async {
    try {
      return await customSqflite.createVehicle(
        DataVehicleModel(
          noPol: noPol,
          condition: condition,
          brand: brand,
          url: url,
          model: model,
          variant: variant,
          manufacture: manufacture,
          mileage: mileage,
          fuelType: fuelType,
          transmission: transmission,
          exterior: exterior,
          price: price,
          notes: notes,
          seller: seller,
          address: address,
          mobileNumber: mobileNumber,
          province: province,
          district: district,
          subDistrict: subDistrict,
          status: status,
          category: category,
          createdAt: createdAt,
          updatedAt: updatedAt,
        ),
      );
    } catch (e) {
      throw const ServerException("Another Failure");
    }
  }

  @override
  Future<VehicleModel> getCurrentVehicle(
    String status,
    String category,
    int offset,
    int limit,
  ) async {
    try {
      return await customSqflite.readVehicle(
        status,
        category,
        offset,
        limit,
      );
    } catch (e) {
      throw const ServerException("Another Failure");
    }
  }
}
