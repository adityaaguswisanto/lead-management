import 'package:lead_management/packages/packages.dart';

abstract class CollectionSource {
  Future<VehicleModel> getCurrentCollection(
    String noPol,
  );
}

class CollectionSourceImpl implements CollectionSource {
  final CustomSqflite customSqflite;

  CollectionSourceImpl({
    required this.customSqflite,
  });

  @override
  Future<VehicleModel> getCurrentCollection(
    String noPol,
  ) async {
    try {
      return await customSqflite.readCollection(
        noPol,
      );
    } catch (e) {
      throw const ServerException("Another Failure");
    }
  }
}
