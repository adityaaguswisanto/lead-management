import 'package:lead_management/packages/packages.dart';

abstract class MachineSource {
  Future<DataVehicleModel> getCurrentMachine(
    String noPol,
  );
  Future<DataVehicleModel> putCurrentMachine(
    DataVehicleModel dataVehicleModel,
  );
}

class MachineSourceImpl implements MachineSource {
  final CustomSqflite customSqflite;

  MachineSourceImpl({
    required this.customSqflite,
  });

  @override
  Future<DataVehicleModel> getCurrentMachine(
    String noPol,
  ) async {
    try {
      return await customSqflite.readMachine(
        noPol,
      );
    } catch (e) {
      throw const ServerException("Another Failure");
    }
  }

  @override
  Future<DataVehicleModel> putCurrentMachine(
    DataVehicleModel dataVehicleModel,
  ) async {
    try {
      return await customSqflite.updateMachine(
        dataVehicleModel,
      );
    } catch (e) {
      throw const ServerException("Another Failure");
    }
  }
}
