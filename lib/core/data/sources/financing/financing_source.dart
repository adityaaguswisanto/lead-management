import 'package:lead_management/packages/packages.dart';

abstract class FinancingSource {
  Future<FinancingModel> getCurrentFinancing(
    String category,
  );
}

class FinancingSourceImpl implements FinancingSource {
  final CustomSqflite customSqflite;

  FinancingSourceImpl({
    required this.customSqflite,
  });

  @override
  Future<FinancingModel> getCurrentFinancing(
    String category,
  ) async {
    try {
      return await customSqflite.readFinancing(
        category,
      );
    } catch (e) {
      throw const ServerException("Another Failure");
    }
  }
}
