import 'package:lead_management/packages/packages.dart';

class FinancingModel extends Equatable {
  final int listing;
  final int inspecting;
  final int visited;
  final int assigningSurveyor;
  final int surveying;
  final int approval;
  final int purchasingOrder;
  final int rejected;
  final int unitNotAvailable;

  const FinancingModel({
    required this.listing,
    required this.inspecting,
    required this.visited,
    required this.assigningSurveyor,
    required this.surveying,
    required this.approval,
    required this.purchasingOrder,
    required this.rejected,
    required this.unitNotAvailable,
  });

  factory FinancingModel.fromJson(Map<String, dynamic> json) {
    return FinancingModel(
      listing: json["listing"],
      inspecting: json["inspecting"],
      visited: json["visited"],
      assigningSurveyor: json["assigning_surveyor"],
      surveying: json["surveying"],
      approval: json["approval"],
      purchasingOrder: json["purchasing_order"],
      rejected: json["rejected"],
      unitNotAvailable: json["unit_not_available"],
    );
  }

  Map<String, dynamic> toJson() => {
        "listing": listing,
        "inspecting": inspecting,
        "visited": visited,
        "assigning_surveyor": assigningSurveyor,
        "surveying": surveying,
        "approval": approval,
        "purchasing_order": purchasingOrder,
        "rejected": rejected,
        "unit_not_available": unitNotAvailable,
      };

  Financing toEntity() => Financing(
        listing: listing,
        inspecting: inspecting,
        visited: visited,
        assigningSurveyor: assigningSurveyor,
        surveying: surveying,
        approval: approval,
        purchasingOrder: purchasingOrder,
        rejected: rejected,
        unitNotAvailable: unitNotAvailable,
      );

  @override
  List<Object?> get props => [
        listing,
        inspecting,
        visited,
        assigningSurveyor,
        surveying,
        approval,
      ];
}
