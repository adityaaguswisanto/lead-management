import 'package:lead_management/packages/packages.dart';

class DataVehicleModel extends Equatable {
  final String noPol;
  final String condition;
  final String brand;
  final String url;
  final String model;
  final String variant;
  final String manufacture;
  final String mileage;
  final String fuelType;
  final String transmission;
  final String exterior;
  final String price;
  final String notes;
  final String seller;
  final String address;
  final String mobileNumber;
  final String province;
  final String district;
  final String subDistrict;
  final String status;
  final String category;
  final int createdAt;
  final int updatedAt;

  const DataVehicleModel({
    required this.noPol,
    required this.condition,
    required this.brand,
    required this.url,
    required this.model,
    required this.variant,
    required this.manufacture,
    required this.mileage,
    required this.fuelType,
    required this.transmission,
    required this.exterior,
    required this.price,
    required this.notes,
    required this.seller,
    required this.address,
    required this.mobileNumber,
    required this.province,
    required this.district,
    required this.subDistrict,
    required this.status,
    required this.category,
    required this.createdAt,
    required this.updatedAt,
  });

  factory DataVehicleModel.fromJson(Map<String, dynamic> json) {
    return DataVehicleModel(
      noPol: json["no_pol"],
      condition: json["condition"],
      brand: json["brand"],
      url: json["url"],
      model: json["model"],
      variant: json["variant"],
      manufacture: json["manufacture"],
      mileage: json["mileage"],
      fuelType: json["fuel_type"],
      transmission: json["transmission"],
      exterior: json["exterior"],
      price: json["price"],
      notes: json["notes"],
      seller: json["seller"],
      address: json["address"],
      mobileNumber: json["mobile_number"],
      province: json["province"],
      district: json["district"],
      subDistrict: json["sub_district"],
      status: json["status"],
      category: json["category"],
      createdAt: json["created_at"],
      updatedAt: json["updated_at"],
    );
  }

  Map<String, dynamic> toJson() => {
        "no_pol": noPol,
        "condition": condition,
        "brand": brand,
        "url": url,
        "model": model,
        "variant": variant,
        "manufacture": manufacture,
        "mileage": mileage,
        "fuel_type": fuelType,
        "transmission": transmission,
        "exterior": exterior,
        "price": price,
        "notes": notes,
        "seller": seller,
        "address": address,
        "mobile_number": mobileNumber,
        "province": province,
        "district": district,
        "sub_district": subDistrict,
        "status": status,
        "category": category,
        "created_at": createdAt,
        "updated_at": updatedAt,
      };

  DataVehicle toEntity() => DataVehicle(
        noPol: noPol,
        condition: condition,
        brand: brand,
        url: url,
        model: model,
        variant: variant,
        manufacture: manufacture,
        mileage: mileage,
        fuelType: fuelType,
        transmission: transmission,
        exterior: exterior,
        price: price,
        notes: notes,
        seller: seller,
        address: address,
        mobileNumber: mobileNumber,
        province: province,
        district: district,
        subDistrict: subDistrict,
        status: status,
        category: category,
        createdAt: createdAt,
        updatedAt: updatedAt,
      );

  @override
  List<Object?> get props => [
        noPol,
        condition,
        brand,
        url,
        model,
        variant,
        manufacture,
        mileage,
        fuelType,
        transmission,
        exterior,
        price,
        notes,
        seller,
        address,
        mobileNumber,
        province,
        district,
        subDistrict,
        status,
        category,
        createdAt,
        updatedAt,
      ];
}
