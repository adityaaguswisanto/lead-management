import 'package:lead_management/packages/packages.dart';

const String vehicleTable = 'vehicle';

class VehicleModel extends Equatable {
  final List<DataVehicleModel> dataVehicle;

  const VehicleModel({
    required this.dataVehicle,
  });

  factory VehicleModel.fromJson(Map<String, dynamic> json) {
    return VehicleModel(
      dataVehicle: List<DataVehicleModel>.from(
        json["data_vehicle"].map(
          (x) => DataVehicleModel.fromJson(x),
        ),
      ),
    );
  }

  Map<String, dynamic> toJson() => {
        "data_vehicle": dataVehicle,
      };

  Vehicle toEntity() => Vehicle(
        dataVehicle: dataVehicle
            .map(
              (e) => DataVehicle(
                noPol: e.noPol,
                condition: e.condition,
                brand: e.brand,
                url: e.url,
                model: e.model,
                variant: e.variant,
                manufacture: e.manufacture,
                mileage: e.mileage,
                fuelType: e.fuelType,
                transmission: e.transmission,
                exterior: e.exterior,
                price: e.price,
                notes: e.notes,
                seller: e.seller,
                address: e.address,
                mobileNumber: e.mobileNumber,
                province: e.province,
                district: e.district,
                subDistrict: e.subDistrict,
                status: e.status,
                category: e.category,
                createdAt: e.createdAt,
                updatedAt: e.updatedAt,
              ),
            )
            .toList(),
      );

  @override
  List<Object?> get props => [
        dataVehicle,
      ];
}
