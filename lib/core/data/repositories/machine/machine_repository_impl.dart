import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

class MachineRepositoryImpl implements MachineRepository {
  final MachineSource machineSource;

  const MachineRepositoryImpl({
    required this.machineSource,
  });

  @override
  Future<Either<Failure, DataVehicle>> getCurrentMachine(
    String noPol,
  ) async {
    try {
      final result = await machineSource.getCurrentMachine(
        noPol,
      );
      return Right(
        result.toEntity(),
      );
    } on ServerException catch (e) {
      return Left(ServerFailure(e.error.toString()));
    }
  }

  @override
  Future<Either<Failure, DataVehicle>> putCurrentMachine(
    DataVehicleModel dataVehicleModel,
  ) async {
    try {
      final result = await machineSource.putCurrentMachine(
        dataVehicleModel,
      );
      return Right(
        result.toEntity(),
      );
    } on ServerException catch (e) {
      return Left(ServerFailure(e.error.toString()));
    }
  }
}
