import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

class CollectionRepositoryImpl implements CollectionRepository {
  final CollectionSource collectionSource;

  const CollectionRepositoryImpl({
    required this.collectionSource,
  });

  @override
  Future<Either<Failure, Vehicle>> getCurrentCollection(
    String noPol,
  ) async {
    try {
      final result = await collectionSource.getCurrentCollection(
        noPol,
      );
      return Right(
        result.toEntity(),
      );
    } on ServerException catch (e) {
      return Left(ServerFailure(e.error.toString()));
    }
  }
}
