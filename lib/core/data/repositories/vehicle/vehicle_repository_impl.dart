import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

class VehicleRepositoryImpl implements VehicleRepository {
  final VehicleSource vehicleSource;

  const VehicleRepositoryImpl({
    required this.vehicleSource,
  });

  @override
  Future<Either<Failure, DataVehicle>> postCurrentVehicle(
    String noPol,
    String condition,
    String brand,
    String url,
    String model,
    String variant,
    String manufacture,
    String mileage,
    String fuelType,
    String transmission,
    String exterior,
    String price,
    String notes,
    String seller,
    String address,
    String mobileNumber,
    String province,
    String district,
    String subDistrict,
    String status,
    String category,
    int createdAt,
    int updatedAt,
  ) async {
    try {
      final result = await vehicleSource.postCurrentVehicle(
        noPol,
        condition,
        brand,
        url,
        model,
        variant,
        manufacture,
        mileage,
        fuelType,
        transmission,
        exterior,
        price,
        notes,
        seller,
        address,
        mobileNumber,
        province,
        district,
        subDistrict,
        status,
        category,
        createdAt,
        updatedAt,
      );
      return Right(
        result.toEntity(),
      );
    } on ServerException catch (e) {
      return Left(ServerFailure(e.error.toString()));
    }
  }

  @override
  Future<Either<Failure, Vehicle>> getCurrentVehicle(
    String status,
    String category,
    int offset,
    int limit,
  ) async {
    try {
      final result = await vehicleSource.getCurrentVehicle(
        status,
        category,
        offset,
        limit,
      );
      return Right(
        result.toEntity(),
      );
    } on ServerException catch (e) {
      return Left(ServerFailure(e.error.toString()));
    }
  }
}
