import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

class FinancingRepositoryImpl implements FinancingRepository {
  final FinancingSource financingSource;

  const FinancingRepositoryImpl({
    required this.financingSource,
  });

  @override
  Future<Either<Failure, Financing>> getCurrentFinancing(
    String category,
  ) async {
    try {
      final result = await financingSource.getCurrentFinancing(
        category,
      );
      return Right(
        result.toEntity(),
      );
    } on ServerException catch (e) {
      return Left(ServerFailure(e.error.toString()));
    }
  }
}
