import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

abstract class FinancingRepository {
  Future<Either<Failure, Financing>> getCurrentFinancing(
    String category,
  );
}
