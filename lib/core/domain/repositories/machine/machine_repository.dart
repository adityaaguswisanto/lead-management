import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

abstract class MachineRepository {
  Future<Either<Failure, DataVehicle>> getCurrentMachine(
    String noPol,
  );

  Future<Either<Failure, DataVehicle>> putCurrentMachine(
    DataVehicleModel dataVehicleModel,
  );
}
