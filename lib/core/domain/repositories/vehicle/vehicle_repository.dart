import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

abstract class VehicleRepository {
  Future<Either<Failure, DataVehicle>> postCurrentVehicle(
    String noPol,
    String condition,
    String brand,
    String url,
    String model,
    String variant,
    String manufacture,
    String mileage,
    String fuelType,
    String transmission,
    String exterior,
    String price,
    String notes,
    String seller,
    String address,
    String mobileNumber,
    String province,
    String district,
    String subDistrict,
    String status,
    String category,
    int createdAt,
    int updatedAt,
  );

  Future<Either<Failure, Vehicle>> getCurrentVehicle(
    String status,
    String category,
    int offset,
    int limit,
  );
}
