import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

abstract class CollectionRepository {
  Future<Either<Failure, Vehicle>> getCurrentCollection(
    String noPol,
  );
}
