import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

class GetCurrentCollection {
  final CollectionRepository collectionRepository;

  GetCurrentCollection(
    this.collectionRepository,
  );

  Future<Either<Failure, Vehicle>> get(String noPol) {
    return collectionRepository.getCurrentCollection(
      noPol,
    );
  }
}
