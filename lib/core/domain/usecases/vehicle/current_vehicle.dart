import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

class GetCurrentVehicle {
  final VehicleRepository vehicleRepository;

  GetCurrentVehicle(
    this.vehicleRepository,
  );

  Future<Either<Failure, DataVehicle>> post(
    String noPol,
    String condition,
    String brand,
    String url,
    String model,
    String variant,
    String manufacture,
    String mileage,
    String fuelType,
    String transmission,
    String exterior,
    String price,
    String notes,
    String seller,
    String address,
    String mobileNumber,
    String province,
    String district,
    String subDistrict,
    String status,
    String category,
    int createdAt,
    int updatedAt,
  ) {
    return vehicleRepository.postCurrentVehicle(
      noPol,
      condition,
      brand,
      url,
      model,
      variant,
      manufacture,
      mileage,
      fuelType,
      transmission,
      exterior,
      price,
      notes,
      seller,
      address,
      mobileNumber,
      province,
      district,
      subDistrict,
      status,
      category,
      createdAt,
      updatedAt,
    );
  }

  Future<Either<Failure, Vehicle>> get(
    String status,
    String category,
    int offset,
    int limit,
  ) {
    return vehicleRepository.getCurrentVehicle(
      status,
      category,
      offset,
      limit,
    );
  }
}
