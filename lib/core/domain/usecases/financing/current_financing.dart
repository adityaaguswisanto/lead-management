import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

class GetCurrentFinancing {
  final FinancingRepository financingRepository;

  GetCurrentFinancing(
    this.financingRepository,
  );

  Future<Either<Failure, Financing>> get(String category) {
    return financingRepository.getCurrentFinancing(
      category,
    );
  }
}
