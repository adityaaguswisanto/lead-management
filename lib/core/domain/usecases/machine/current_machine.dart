import 'package:dartz/dartz.dart';
import 'package:lead_management/packages/packages.dart';

class GetCurrentMachine {
  final MachineRepository machineRepository;

  GetCurrentMachine(
    this.machineRepository,
  );

  Future<Either<Failure, DataVehicle>> get(
    String noPol,
  ) {
    return machineRepository.getCurrentMachine(
      noPol,
    );
  }

  Future<Either<Failure, DataVehicle>> put(
    DataVehicleModel dataVehicleModel,
  ) {
    return machineRepository.putCurrentMachine(
      dataVehicleModel,
    );
  }
}
