class Manufacture {
  String title;

  Manufacture(
    this.title,
  );
}

List<Manufacture> dataManufacture = [
  Manufacture(
    "2009",
  ),
  Manufacture(
    "2010",
  ),
  Manufacture(
    "2011",
  ),
  Manufacture(
    "2012",
  ),
  Manufacture(
    "2013",
  ),
  Manufacture(
    "2014",
  ),
];
