class District {
  String title;
  String description;

  District(
    this.title,
    this.description,
  );
}

List<District> dataDistrict = [
  District(
    "Bandung",
    "Jawa Barat",
  ),
  District(
    "Semarang",
    "Jawa Tengah",
  ),
  District(
    "Surabaya",
    "Jawa Timur",
  ),
];
