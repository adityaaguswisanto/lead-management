import 'package:lead_management/packages/packages.dart';

export 'data_vehicle.dart';

class Vehicle extends Equatable {
  final List<DataVehicle> dataVehicle;

  const Vehicle({
    required this.dataVehicle,
  });

  @override
  List<Object?> get props => [
        dataVehicle,
      ];
}

class VehicleArgument extends Equatable {
  final int? id;
  final String? category;

  const VehicleArgument({
    required this.id,
    required this.category,
  });

  @override
  List<Object?> get props => [
        id,
        category,
      ];
}

class MenuVehicle {
  int id;
  String title;
  String description;
  String url;

  MenuVehicle(
    this.id,
    this.title,
    this.description,
    this.url,
  );
}

List<MenuVehicle> dataMenuVehicle = [
  MenuVehicle(
    1,
    "Listing",
    "Customer service",
    "assets/images/home/green.png",
  ),
  MenuVehicle(
    2,
    "Inspecting",
    "Marketing officer",
    "assets/images/home/green.png",
  ),
  MenuVehicle(
    3,
    "Visited",
    "Marketing officer",
    "assets/images/home/green.png",
  ),
  MenuVehicle(
    4,
    "Assigning surveyor",
    "Marketing head",
    "assets/images/home/green.png",
  ),
  MenuVehicle(
    5,
    "Surveying",
    "Credit officer",
    "assets/images/home/green.png",
  ),
  MenuVehicle(
    6,
    "Approval",
    "Marketing officer",
    "assets/images/home/green.png",
  ),
  MenuVehicle(
    7,
    "Purchasing order",
    "Marketing officer",
    "assets/images/home/blue.png",
  ),
  MenuVehicle(
    8,
    "Rejected",
    "Marketing officer",
    "assets/images/home/blue.png",
  ),
  MenuVehicle(
    9,
    "Unit not available",
    "Marketing officer",
    "assets/images/home/blue.png",
  ),
];
