class Status {
  int id;
  String title;
  String image;

  Status(
    this.id,
    this.title,
    this.image,
  );
}

List<Status> dataStatus = [
  Status(
    1,
    "Listing",
    "assets/images/machine/purple.png",
  ),
  Status(
    2,
    "Inspecting",
    "assets/images/machine/purple.png",
  ),
  Status(
    3,
    "Visited",
    "assets/images/machine/purple.png",
  ),
  Status(
    4,
    "Assigning surveyor",
    "assets/images/machine/purple.png",
  ),
  Status(
    5,
    "Surveying",
    "assets/images/machine/purple.png",
  ),
  Status(
    6,
    "Approval",
    "assets/images/machine/purple.png",
  ),
  Status(
    7,
    "Purchasing order",
    "assets/images/machine/blue.png",
  ),
  Status(
    8,
    "Rejected",
    "assets/images/machine/blue.png",
  ),
  Status(
    9,
    "Unit not available",
    "assets/images/machine/blue.png",
  ),
];
