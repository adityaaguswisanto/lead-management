class Seller {
  String title;
  String description;

  Seller(
    this.title,
    this.description,
  );
}

List<Seller> dataSeller = [
  Seller(
    "Sumber Makmur",
    "Jl. Magnolia timur 7, bekasi jawa barat",
  ),
  Seller(
    "Sumber Makmur",
    "Jl. kemang pratama 2 hibiskus",
  ),
  Seller(
    "Sumber Makmur",
    "Jl. Kapitan pattimura no 32",
  ),
  Seller(
    "Sumbada mobil bogor",
    "Jl. R.A kartini, bekasi, jawa barat",
  ),
  Seller(
    "Sumbada motor jaya",
    "Jl. hegarmanah timur",
  ),
  Seller(
    "Sumber kemakmuran jaya raya",
    "Jl. andunohu",
  ),
  Seller(
    "Sumber sari mobil",
    "Jl ketapang 8 blok au ah",
  ),
];
