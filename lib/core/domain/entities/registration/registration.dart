import 'package:lead_management/packages/packages.dart';

class Registration {
  String title;
  String description;
  int position;

  Registration(
    this.title,
    this.description,
    this.position,
  );
}

List<Registration> dataRegistration = [
  Registration(
    "Registration number",
    "Please insert vehicle registration number",
    1,
  ),
  Registration(
    "Detail information",
    "Please insert vehicle detail information",
    0,
  ),
];

class RegistrationArgument extends Equatable {
  final String? category;

  const RegistrationArgument({
    required this.category,
  });

  @override
  List<Object?> get props => [
        category,
      ];
}
