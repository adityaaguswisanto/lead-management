class Province {
  String title;

  Province(
    this.title,
  );
}

List<Province> dataProvince = [
  Province(
    "Jawa Barat",
  ),
  Province(
    "Jawa Tengah",
  ),
  Province(
    "Jawa Timur",
  ),
];
