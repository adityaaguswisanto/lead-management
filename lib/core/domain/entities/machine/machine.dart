import 'package:lead_management/packages/packages.dart';

class MachineArgument extends Equatable {
  final int? id;
  final String? noPol;
  final String? category;

  const MachineArgument({
    required this.id,
    required this.noPol,
    required this.category,
  });

  @override
  List<Object?> get props => [
        id,
        noPol,
        category,
      ];
}
