import 'package:lead_management/packages/packages.dart';

class AdvertiseArgument extends Equatable {
  final String? noPol;
  final String? category;

  const AdvertiseArgument({
    required this.noPol,
    required this.category,
  });

  @override
  List<Object?> get props => [
        noPol,
        category,
      ];
}
