import 'package:lead_management/packages/packages.dart';

class Financing extends Equatable {
  final int listing;
  final int inspecting;
  final int visited;
  final int assigningSurveyor;
  final int surveying;
  final int approval;
  final int purchasingOrder;
  final int rejected;
  final int unitNotAvailable;

  const Financing({
    required this.listing,
    required this.inspecting,
    required this.visited,
    required this.assigningSurveyor,
    required this.surveying,
    required this.approval,
    required this.purchasingOrder,
    required this.rejected,
    required this.unitNotAvailable,
  });

  @override
  List<Object?> get props => [
        listing,
        inspecting,
        visited,
        assigningSurveyor,
        surveying,
        approval,
        purchasingOrder,
        rejected,
        unitNotAvailable,
      ];
}
