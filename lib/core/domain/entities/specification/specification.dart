import 'package:lead_management/packages/packages.dart';

class SpecificationArgument extends Equatable {
  final String? noPol;
  final String? category;

  const SpecificationArgument({
    required this.noPol,
    required this.category,
  });

  @override
  List<Object?> get props => [
        noPol,
        category,
      ];
}
