class Brand {
  String title;
  String url;

  Brand(
    this.title,
    this.url,
  );
}

List<Brand> dataBrand = [
  Brand(
    "Suzuki",
    "assets/images/brand/suzuki.png",
  ),
  Brand(
    "Mitsubishi",
    "assets/images/brand/mitsubishi.png",
  ),
];
