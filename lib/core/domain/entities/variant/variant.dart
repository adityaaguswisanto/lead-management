class Variant {
  String title;

  Variant(
    this.title,
  );
}

List<Variant> dataVariant = [
  Variant(
    "3.9 FE 71 Solar",
  ),
  Variant(
    "3.1 FE 40 Premium",
  ),
  Variant(
    "4.0 FE 93 Pertamax",
  ),
];
