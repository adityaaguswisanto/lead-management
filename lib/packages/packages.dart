//local
export 'package:flutter/material.dart';
export 'package:flutter/services.dart';
export 'dart:io';
export 'dart:convert';
export 'package:flutter/gestures.dart';

//packages
export 'package:get_it/get_it.dart';
export 'package:flutter_bloc/flutter_bloc.dart';
export 'package:equatable/equatable.dart';
export 'package:sqflite/sqflite.dart';
export 'package:flutter_screenutil/flutter_screenutil.dart';
export 'package:fluttertoast/fluttertoast.dart';
export 'package:mockito/annotations.dart';
export 'package:mockito/mockito.dart';
export 'package:bloc_test/bloc_test.dart';
export 'package:shimmer/shimmer.dart';
export 'package:flutter_svg/flutter_svg.dart';

//route
export 'package:lead_management/core/core.dart';
export 'package:lead_management/modules/modules.dart';
export 'package:lead_management/shared/shared.dart';
export 'package:lead_management/route_generator.dart';
