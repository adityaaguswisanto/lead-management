import 'package:lead_management/packages/packages.dart';

class RegistrationItem extends StatelessWidget {
  final Registration registration;

  const RegistrationItem({
    super.key,
    required this.registration,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          children: [
            Container(
              width: 30.w,
              height: 30.h,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(
                  width: 4,
                  color: registration.position == 1
                      ? CustomColorStyle.green.withOpacity(
                          0.1,
                        )
                      : CustomColorStyle.transparent,
                ),
                color: registration.position == 1
                    ? CustomColorStyle.green.withOpacity(
                        0.2,
                      )
                    : CustomColorStyle.black.withOpacity(
                        0.06,
                      ),
              ),
              child: Container(
                margin: const EdgeInsets.all(
                  8,
                ),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: registration.position == 1
                      ? CustomColorStyle.green
                      : CustomColorStyle.black.withOpacity(
                          0.2,
                        ),
                ),
              ),
            ),
            Container(
              width: 2.w,
              margin: const EdgeInsets.only(
                bottom: 10,
              ),
              height: CustomSize.height(context) / 24,
              color: CustomColorStyle.black.withOpacity(
                0.1,
              ),
            ),
          ],
        ),
        SizedBox(
          width: 8.w,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 8.h,
              ),
              Text(
                registration.title,
                style: CustomTextStyle.bold(
                  12.sp,
                  color: registration.position == 1
                      ? CustomColorStyle.green
                      : CustomColorStyle.fontBlack,
                ),
              ),
              Text(
                registration.description,
                style: CustomTextStyle.regular(
                  12.sp,
                  color: registration.position == 1
                      ? CustomColorStyle.green
                      : CustomColorStyle.fontBlack,
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
