import 'dart:ui';

import 'package:lead_management/packages/packages.dart';

class RegistrationDialog extends StatelessWidget {
  final TextEditingController noPol;
  final String category;

  const RegistrationDialog({
    super.key,
    required this.noPol,
    required this.category,
  });

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
      child: Dialog(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            10,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomInfo(
                    width: 60.w,
                    height: 60.h,
                    borderColor: CustomColorStyle.yellowWhite,
                    backgroundColor: CustomColorStyle.yellow.withOpacity(
                      0.4,
                    ),
                    iconColor: CustomColorStyle.yellowDark,
                  ),
                  IconButton(
                    onPressed: () => CustomNavigation.back(context),
                    icon: Icon(
                      Icons.close,
                      color: CustomColorStyle.black.withOpacity(
                        0.4,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                noPol.text.toString(),
                style: CustomTextStyle.regular(
                  16.sp,
                ),
              ),
              SizedBox(
                height: 8.h,
              ),
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text:
                          "Make sure that the number you put in is correct, because it'll be ",
                      style: CustomTextStyle.regular(
                        12.sp,
                        color: CustomColorStyle.black.withOpacity(
                          0.9,
                        ),
                      ),
                    ),
                    TextSpan(
                      text: "unreplaceable ",
                      style: CustomTextStyle.bold(
                        12.sp,
                        color: CustomColorStyle.red,
                      ),
                    ),
                    TextSpan(
                      text: "after you clicking the next button down below",
                      style: CustomTextStyle.regular(
                        12.sp,
                        color: CustomColorStyle.black.withOpacity(
                          0.9,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 12.h,
              ),
              SizedBox(
                width: CustomSize.width(context),
                child: CustomButton(
                  onPressed: () {
                    CustomNavigation.intentWithoutBack(
                      context,
                      SpecificationPage.routeName,
                      arguments: SpecificationArgument(
                        noPol: noPol.text,
                        category: category,
                      ),
                    );
                    noPol.text = "";
                  },
                  label: "Next",
                ),
              ),
              SizedBox(
                height: 6.h,
              ),
              SizedBox(
                width: CustomSize.width(context),
                child: CustomButton(
                  onPressed: () => CustomNavigation.back(context),
                  label: "Cancel",
                  withOutline: true,
                ),
              ),
              SizedBox(
                height: 16.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
