import 'package:lead_management/packages/packages.dart';

class RegistrationPage extends StatefulWidget {
  static const routeName = "/registration";

  final RegistrationArgument registrationArgument;

  const RegistrationPage({
    super.key,
    required this.registrationArgument,
  });

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  final formKey = GlobalKey<FormState>();
  final noPol = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(
            16,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomBack(
                title: "Home",
                onPopInvoked: (bool didPop) =>
                    CustomNavigation.intentWithClearAllRoutes(
                  context,
                  HomePage.routeName,
                ),
                onTap: () => CustomNavigation.intentWithClearAllRoutes(
                  context,
                  HomePage.routeName,
                ),
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                "Registration Mobile*",
                style: CustomTextStyle.medium(
                  16.sp,
                ),
              ),
              SizedBox(
                height: 4.h,
              ),
              Text(
                "Please insert vehicle registration number.",
                style: CustomTextStyle.medium(
                  14.sp,
                  color: CustomColorStyle.black.withOpacity(
                    0.6,
                  ),
                ),
              ),
              SizedBox(
                height: 16.h,
              ),
              Form(
                key: formKey,
                child: TextFormField(
                  controller: noPol,
                  textCapitalization: TextCapitalization.characters,
                  inputFormatters: [
                    CustomNopolFormatter(),
                  ],
                  validator: (value) => CustomValidation.empty(
                    "No. Police",
                    value.toString(),
                  ),
                  decoration: CustomFieldStyle.regular(
                    "No. Police in here",
                  ),
                  style: CustomTextStyle.medium(
                    12.sp,
                  ),
                ),
              ),
              SizedBox(
                height: 6.h,
              ),
              SizedBox(
                width: CustomSize.width(context),
                child: CustomButton(
                  onPressed: () async {
                    if (formKey.currentState!.validate()) {
                      showDialog(
                        context: context,
                        builder: (context) {
                          return RegistrationDialog(
                            noPol: noPol,
                            category:
                                widget.registrationArgument.category.toString(),
                          );
                        },
                      );
                    }
                  },
                  label: "Next",
                ),
              ),
              SizedBox(
                height: 16.h,
              ),
              ListView.builder(
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(),
                itemCount: dataRegistration.length,
                itemBuilder: (context, index) {
                  return RegistrationItem(
                    registration: dataRegistration[index],
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
