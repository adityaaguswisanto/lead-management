import 'package:lead_management/packages/packages.dart';

class HomePage extends StatefulWidget {
  static const routeName = "/home";

  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final customSqflite = locator<CustomSqflite>();
  final financingBloc = locator<FinancingBloc>();
  final refinancingBloc = locator<FinancingBloc>();

  bool financing = true;
  bool refinancing = false;

  @override
  void dispose() {
    financingBloc.close();
    super.dispose();
  }

  @override
  void initState() {
    financingBloc.add(
      const FinancingGetted(
        category: "financing",
      ),
    );
    refinancingBloc.add(
      const FinancingGetted(
        category: "refinancing",
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const HomeAppBar(),
              Divider(
                height: 2.h,
                color: CustomColorStyle.black.withOpacity(
                  0.1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(
                  16,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Welcome Back, Muammar",
                      style: CustomTextStyle.medium(
                        20.sp,
                      ),
                    ),
                    Text(
                      "Your leads summary and activity",
                      style: CustomTextStyle.regular(
                        14.sp,
                        color: CustomColorStyle.black.withOpacity(
                          0.6,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 16.h,
                    ),
                    HomeTab(
                      onTapFinancing: () => setState(() {
                        financing = true;
                        refinancing = false;
                      }),
                      onTapReFinancing: () => setState(() {
                        refinancing = true;
                        financing = false;
                      }),
                      financing: financing,
                      refinancing: refinancing,
                    ),
                    SizedBox(
                      height: 16.h,
                    ),
                    BlocBuilder(
                      bloc: financingBloc,
                      builder: (context, state) {
                        if (state is FinancingLoading) {
                          return const HomeOpenShimmer();
                        } else if (state is FinancingGetSuccess) {
                          return Visibility(
                            visible: financing,
                            child: HomeFinancing(
                              financingBloc: financingBloc,
                              financing: state.financing,
                              category: "financing",
                            ),
                          );
                        } else if (state is FinancingFailure) {
                          return CustomHandling(
                            withError: true,
                            onTap: () {
                              financingBloc.add(
                                const FinancingGetted(
                                  category: "financing",
                                ),
                              );
                            },
                          );
                        }
                        return const SizedBox.shrink();
                      },
                    ),
                    BlocBuilder(
                      bloc: refinancingBloc,
                      builder: (context, state) {
                        if (state is FinancingLoading) {
                          return const HomeCloseShimmer();
                        } else if (state is FinancingGetSuccess) {
                          return Visibility(
                            visible: refinancing,
                            child: HomeFinancing(
                              financingBloc: refinancingBloc,
                              financing: state.financing,
                              category: "refinancing",
                            ),
                          );
                        } else if (state is FinancingFailure) {
                          return CustomHandling(
                            withError: true,
                            onTap: () {
                              financingBloc.add(
                                const FinancingGetted(
                                  category: "refinancing",
                                ),
                              );
                            },
                          );
                        }
                        return const SizedBox.shrink();
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
