import 'package:lead_management/packages/packages.dart';

class HomeOpenShimmer extends StatelessWidget {
  const HomeOpenShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomShimmer(
          width: CustomSize.width(context) / 8,
          height: 16.h,
        ),
        SizedBox(
          height: 4.h,
        ),
        CustomShimmer(
          width: CustomSize.width(context) / 4,
          height: 14.h,
        ),
        SizedBox(
          height: 16.h,
        ),
        CustomShimmer(
          width: CustomSize.width(context) / 2,
          height: 30.h,
        ),
        ListView.builder(
          shrinkWrap: true,
          padding: const EdgeInsets.only(
            top: 26,
          ),
          physics: const BouncingScrollPhysics(),
          itemCount: 6,
          itemBuilder: (context, index) {
            return Column(
              children: [
                Divider(
                  height: 2,
                  color: CustomColorStyle.black.withOpacity(
                    0.1,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 12,
                  ),
                  child: Row(
                    children: [
                      CustomShimmer(
                        width: 34.h,
                        height: 34.h,
                        withCircle: true,
                      ),
                      SizedBox(
                        width: 8.h,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomShimmer(
                              width: CustomSize.width(context) / 4,
                              height: 14.h,
                            ),
                            SizedBox(
                              height: 2.h,
                            ),
                            CustomShimmer(
                              width: CustomSize.width(context) / 2,
                              height: 14.h,
                            ),
                          ],
                        ),
                      ),
                      CustomShimmer(
                        width: CustomSize.width(context) / 16,
                        height: 14.h,
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ],
    );
  }
}
