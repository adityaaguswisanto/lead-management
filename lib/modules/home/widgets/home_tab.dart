import 'package:lead_management/packages/packages.dart';

class HomeTab extends StatelessWidget {
  final GestureTapCallback onTapFinancing;
  final GestureTapCallback onTapReFinancing;
  final bool financing;
  final bool refinancing;

  const HomeTab({
    super.key,
    required this.onTapFinancing,
    required this.onTapReFinancing,
    required this.financing,
    required this.refinancing,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned(
          left: 0,
          right: 0,
          bottom: 0,
          child: Container(
            width: CustomSize.width(context),
            height: 2,
            color: CustomColorStyle.black.withOpacity(
              0.2,
            ),
          ),
        ),
        Row(
          children: [
            CustomTab(
              title: "Financing",
              onTap: onTapFinancing,
              active: financing,
              status: true,
            ),
            SizedBox(
              width: 16.w,
            ),
            CustomTab(
              title: "Refinancing",
              onTap: onTapReFinancing,
              active: refinancing,
              status: true,
            ),
          ],
        )
      ],
    );
  }
}
