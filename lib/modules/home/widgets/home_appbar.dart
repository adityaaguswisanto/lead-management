import 'package:lead_management/packages/packages.dart';

class HomeAppBar extends StatelessWidget {
  const HomeAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 16,
        horizontal: 16,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(
            "assets/images/dashboard/logo.png",
            width: 120.w,
          ),
          Row(
            children: [
              Image.asset(
                "assets/images/dashboard/burger.png",
                width: 20.w,
                height: 20.w,
              ),
              SizedBox(
                width: 16.w,
              ),
              GestureDetector(
                onTap: () => CustomNavigation.intent(
                  context,
                  CollectionPage.routeName,
                ),
                child: Icon(
                  Icons.search,
                  color: CustomColorStyle.black.withOpacity(
                    0.6,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
