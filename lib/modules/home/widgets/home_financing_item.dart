import 'package:lead_management/packages/packages.dart';

class HomeFinancingItem extends StatelessWidget {
  final MenuVehicle menuVehicle;
  final Financing financing;
  final String category;

  const HomeFinancingItem({
    super.key,
    required this.menuVehicle,
    required this.financing,
    required this.category,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => CustomNavigation.intentWithData(
        context,
        VehiclePage.routeName,
        VehicleArgument(
          id: menuVehicle.id,
          category: category,
        ),
      ),
      child: Column(
        children: [
          Divider(
            height: 2,
            color: CustomColorStyle.black.withOpacity(
              0.1,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 12,
            ),
            child: Row(
              children: [
                Image.asset(
                  menuVehicle.url,
                ),
                SizedBox(
                  width: 8.w,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        menuVehicle.title,
                        style: CustomTextStyle.medium(
                          14.sp,
                        ),
                      ),
                      Text(
                        menuVehicle.description,
                        style: CustomTextStyle.medium(
                          12.sp,
                          color: CustomColorStyle.black.withOpacity(
                            0.8,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Text(
                  CustomConvert.decimal(
                    menuVehicle.id == 1
                        ? financing.listing
                        : menuVehicle.id == 2
                            ? financing.inspecting
                            : menuVehicle.id == 3
                                ? financing.visited
                                : menuVehicle.id == 4
                                    ? financing.assigningSurveyor
                                    : menuVehicle.id == 5
                                        ? financing.surveying
                                        : menuVehicle.id == 6
                                            ? financing.approval
                                            : menuVehicle.id == 7
                                                ? financing.purchasingOrder
                                                : menuVehicle.id == 8
                                                    ? financing.rejected
                                                    : financing
                                                        .unitNotAvailable,
                  ),
                  style: CustomTextStyle.medium(
                    12.sp,
                    color: CustomColorStyle.black.withOpacity(
                      0.8,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
