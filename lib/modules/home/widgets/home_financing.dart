import 'package:lead_management/packages/packages.dart';

class HomeFinancing extends StatelessWidget {
  final FinancingBloc financingBloc;
  final Financing financing;
  final String category;

  const HomeFinancing({
    super.key,
    required this.financingBloc,
    required this.financing,
    required this.category,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "OPEN",
          style: CustomTextStyle.medium(
            14.sp,
          ),
        ),
        Text(
          "Leads in process",
          style: CustomTextStyle.regular(
            12.sp,
            color: CustomColorStyle.black.withOpacity(
              0.6,
            ),
          ),
        ),
        SizedBox(
          height: 12.h,
        ),
        CustomButton(
          onPressed: () => CustomNavigation.intentWithData(
            context,
            RegistrationPage.routeName,
            RegistrationArgument(
              category: category,
            ),
          ),
          label:
              "New ${category == "financing" ? "financing" : "refinancing"} lead",
          withIcon: true,
          iconData: Icons.add,
        ),
        ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          padding: const EdgeInsets.only(
            top: 24,
          ),
          itemCount: dataMenuVehicle.sublist(dataMenuVehicle.length - 6).length,
          itemBuilder: (context, index) {
            return HomeFinancingItem(
              menuVehicle: dataMenuVehicle[index],
              financing: financing,
              category: category,
            );
          },
        ),
        Divider(
          height: 2,
          color: CustomColorStyle.black.withOpacity(
            0.1,
          ),
        ),
        SizedBox(
          height: 12.h,
        ),
        Text(
          "CLOSED",
          style: CustomTextStyle.regular(
            14.sp,
          ),
        ),
        Text(
          "Leads complete",
          style: CustomTextStyle.regular(
            12.sp,
            color: CustomColorStyle.black.withOpacity(
              0.6,
            ),
          ),
        ),
        ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          padding: const EdgeInsets.only(
            top: 24,
          ),
          itemCount: dataMenuVehicle.length >= 3 ? 3 : dataMenuVehicle.length,
          itemBuilder: (context, index) {
            return HomeFinancingItem(
              menuVehicle: dataMenuVehicle[dataMenuVehicle.length - 3 + index],
              financing: financing,
              category: category,
            );
          },
        ),
      ],
    );
  }
}
