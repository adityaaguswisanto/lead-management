import 'package:lead_management/packages/packages.dart';

class VehicleFilterItem extends StatelessWidget {
  final int index;
  final int idSelected;
  final ValueChanged<bool>? onSelected;

  const VehicleFilterItem({
    super.key,
    required this.index,
    required this.idSelected,
    required this.onSelected,
  });

  @override
  Widget build(BuildContext context) {
    return ChoiceChip(
      showCheckmark: false,
      selectedColor: CustomColorStyle.black.withOpacity(
        0.1,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(index == 0 ? 8 : 0),
          bottomLeft: Radius.circular(index == 0 ? 8 : 0),
          topRight:
              Radius.circular(index == dataMenuVehicle.length - 1 ? 8 : 0),
          bottomRight:
              Radius.circular(index == dataMenuVehicle.length - 1 ? 8 : 0),
        ),
        side: BorderSide(
          color: CustomColorStyle.black.withOpacity(
            0.06,
          ),
        ),
      ),
      label: Text(
        dataMenuVehicle[index].title.toString(),
        style: CustomTextStyle.medium(
          12.sp,
        ),
      ),
      selected: idSelected == dataMenuVehicle[index].id,
      onSelected: onSelected,
    );
  }
}
