import 'package:lead_management/packages/packages.dart';

class VehicleMore extends StatelessWidget {
  final List<DataVehicle> listDataVehicle;
  final bool showMore;
  final GestureTapCallback? onTap;
  final int limit;

  const VehicleMore({
    super.key,
    required this.listDataVehicle,
    required this.showMore,
    required this.onTap,
    required this.limit,
  });

  @override
  Widget build(BuildContext context) {
    return listDataVehicle.isNotEmpty &&
            showMore == true &&
            listDataVehicle.length >= limit
        ? GestureDetector(
            onTap: onTap,
            child: Text(
              "Tap to load more",
              style: CustomTextStyle.medium(
                12.sp,
                color: CustomColorStyle.green,
              ),
              textAlign: TextAlign.center,
            ),
          )
        : const SizedBox.shrink();
  }
}
