import 'package:lead_management/packages/packages.dart';

class VehiclePage extends StatefulWidget {
  static const routeName = "/vehicle";

  final VehicleArgument vehicleArgument;

  const VehiclePage({
    super.key,
    required this.vehicleArgument,
  });

  @override
  State<VehiclePage> createState() => _VehiclePageState();
}

class _VehiclePageState extends State<VehiclePage> {
  final vehicleBloc = locator<VehicleBloc>();
  final vehicleNextBloc = locator<VehicleBloc>();

  final scrollController = ScrollController();

  late int idSelected = widget.vehicleArgument.id!;

  List<DataVehicle> listDataVehicle = [];

  int offset = 0;
  int limit = 10;
  bool? showMore = true;

  @override
  void dispose() {
    vehicleBloc.close();
    vehicleNextBloc.close();
    super.dispose();
  }

  @override
  void initState() {
    vehicleBloc.add(
      VehicleGetted(
        status: widget.vehicleArgument.id.toString(),
        category: widget.vehicleArgument.category.toString(),
        offset: offset,
        limit: limit,
      ),
    );

    scrollWhenMax();

    super.initState();
  }

  Future<void> scrollWhenMax() async {
    if (idSelected >= 4) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        scrollController.animateTo(
          idSelected <= 6
              ? scrollController.position.maxScrollExtent / 2.4
              : scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeOut,
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocListener(
          bloc: vehicleNextBloc,
          listener: (context, state) {
            if (state is VehicleLoading) {
              CustomToast.regular(
                "On Loading....",
              );
            } else if (state is VehicleGetSuccess) {
              setState(() {
                if (state.vehicle.dataVehicle.isEmpty) {
                  showMore = false;
                  CustomToast.regular(
                    "No data loaded",
                  );
                } else {
                  offset += limit;
                  listDataVehicle.addAll(state.vehicle.dataVehicle);
                }
              });
            } else if (state is VehicleFailure) {
              CustomToast.failure(
                "Another error",
              );
            }
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(
                  16,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomBack(
                      title: "Home",
                      onPopInvoked: (bool didPop) =>
                          CustomNavigation.intentWithClearAllRoutes(
                        context,
                        HomePage.routeName,
                      ),
                      onTap: () => CustomNavigation.intentWithClearAllRoutes(
                        context,
                        HomePage.routeName,
                      ),
                    ),
                    SizedBox(
                      height: 8.h,
                    ),
                    Text(
                      widget.vehicleArgument.category == "financing"
                          ? "Financing"
                          : "Refinancing",
                      style: CustomTextStyle.medium(
                        16.sp,
                      ),
                    ),
                    SizedBox(
                      height: 4.h,
                    ),
                    Text(
                      "Select a card based on pipeline, last update, & seller",
                      style: CustomTextStyle.medium(
                        14.sp,
                        color: CustomColorStyle.black.withOpacity(
                          0.6,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: CustomSize.width(context),
                height: CustomSize.height(context) / 22,
                child: ListView.builder(
                  controller: scrollController,
                  shrinkWrap: true,
                  physics: const BouncingScrollPhysics(),
                  padding: const EdgeInsets.only(
                    left: 16,
                    right: 16,
                  ),
                  scrollDirection: Axis.horizontal,
                  itemCount: dataMenuVehicle.length,
                  itemBuilder: (context, index) {
                    return VehicleFilterItem(
                      index: index,
                      idSelected: idSelected,
                      onSelected: (_) => setState(
                        () {
                          idSelected = dataMenuVehicle[index].id;
                          offset = 0;
                          limit = 10;
                          showMore = true;
                          vehicleBloc.add(
                            VehicleGetted(
                              status: idSelected.toString(),
                              category:
                                  widget.vehicleArgument.category.toString(),
                              offset: offset,
                              limit: limit,
                            ),
                          );
                        },
                      ),
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16,
                  vertical: 24,
                ),
                child: Divider(
                  height: 2,
                  color: CustomColorStyle.black.withOpacity(
                    0.2,
                  ),
                ),
              ),
              Expanded(
                child: BlocBuilder(
                  bloc: vehicleBloc,
                  builder: (context, state) {
                    if (state is VehicleLoading) {
                      return const VehicleShimmer();
                    } else if (state is VehicleGetSuccess) {
                      listDataVehicle = state.vehicle.dataVehicle;
                      if (listDataVehicle.isEmpty) {
                        return CustomHandling(
                          onTap: () {
                            offset = 0;
                            limit = 10;
                            showMore = true;
                            vehicleBloc.add(
                              VehicleGetted(
                                status: idSelected.toString(),
                                category:
                                    widget.vehicleArgument.category.toString(),
                                offset: offset,
                                limit: limit,
                              ),
                            );
                          },
                        );
                      }
                      return ListView.builder(
                        shrinkWrap: true,
                        physics: const BouncingScrollPhysics(),
                        padding: const EdgeInsets.symmetric(
                          vertical: 8,
                          horizontal: 16,
                        ),
                        itemCount: listDataVehicle.length + 1,
                        itemBuilder: (context, index) {
                          if (index < listDataVehicle.length) {
                            return VehicleItem(
                              dataVehicle: listDataVehicle[index],
                              category:
                                  widget.vehicleArgument.category.toString(),
                              idSelected: idSelected,
                            );
                          } else {
                            return VehicleMore(
                              listDataVehicle: listDataVehicle,
                              showMore: showMore!,
                              limit: limit,
                              onTap: () {
                                vehicleNextBloc.add(
                                  VehicleGetted(
                                    status:
                                        widget.vehicleArgument.id.toString(),
                                    category: widget.vehicleArgument.category
                                        .toString(),
                                    offset: offset += limit,
                                    limit: limit,
                                  ),
                                );
                              },
                            );
                          }
                        },
                      );
                    } else if (state is VehicleFailure) {
                      return CustomHandling(
                        withError: true,
                        onTap: () {
                          offset = 0;
                          limit = 10;
                          showMore = true;
                          vehicleBloc.add(
                            VehicleGetted(
                              status: idSelected.toString(),
                              category:
                                  widget.vehicleArgument.category.toString(),
                              offset: offset,
                              limit: limit,
                            ),
                          );
                        },
                      );
                    }
                    return const SizedBox.shrink();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
