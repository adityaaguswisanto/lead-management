import 'package:lead_management/packages/packages.dart';

class MachinePage extends StatefulWidget {
  static const routeName = "/vehicle/machine";

  final MachineArgument machineArgument;

  const MachinePage({
    super.key,
    required this.machineArgument,
  });

  @override
  State<MachinePage> createState() => _MachinePageState();
}

class _MachinePageState extends State<MachinePage> {
  final machineGetBloc = locator<MachineBloc>();
  final machinePutBloc = locator<MachineBloc>();

  Status? status;

  @override
  void initState() {
    machineGetBloc.add(
      MachineGetted(
        noPol: widget.machineArgument.noPol.toString(),
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: BlocListener(
              bloc: machinePutBloc,
              listener: (context, state) {
                if (state is MachineLoading) {
                  //loading
                } else if (state is MachinePutSuccess) {
                  CustomToast.success(state.message);
                } else if (state is MachineFailure) {
                  CustomToast.failure(state.message);
                }
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(
                      16,
                    ),
                    child: CustomBack(
                      title: widget.machineArgument.category == "financing"
                          ? "Financing"
                          : "Refinancing",
                      onPopInvoked: (bool didPop) =>
                          CustomNavigation.intentWithClearAllRoutes(
                        context,
                        VehiclePage.routeName,
                        arguments: VehicleArgument(
                          id: status?.id == null
                              ? widget.machineArgument.id
                              : status!.id,
                          category: widget.machineArgument.category.toString(),
                        ),
                      ),
                      onTap: () => CustomNavigation.intentWithClearAllRoutes(
                        context,
                        VehiclePage.routeName,
                        arguments: VehicleArgument(
                          id: status?.id == null
                              ? widget.machineArgument.id
                              : status!.id,
                          category: widget.machineArgument.category.toString(),
                        ),
                      ),
                    ),
                  ),
                  BlocBuilder(
                    bloc: machineGetBloc,
                    builder: (context, state) {
                      if (state is MachineLoading) {
                        return const MachineShimmer();
                      } else if (state is MachineGetSuccess) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 16,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "${state.dataVehicle.brand} ${state.dataVehicle.model} ${state.dataVehicle.manufacture}",
                                    style: CustomTextStyle.medium(
                                      16.sp,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 4.h,
                                  ),
                                  Text(
                                    "#${state.dataVehicle.noPol.replaceAll(' ', '')}",
                                    style: CustomTextStyle.medium(
                                      12.sp,
                                      color: CustomColorStyle.black.withOpacity(
                                        0.6,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 16.h,
                                  ),
                                  CustomDropdown(
                                    label: "Listing",
                                    value: status,
                                    isExpanded: true,
                                    withFlag: true,
                                    items: dataStatus.map((value) {
                                      return DropdownMenuItem(
                                        value: value,
                                        child: Row(
                                          children: [
                                            Image.asset(
                                              value.image,
                                            ),
                                            SizedBox(
                                              width: 8.w,
                                            ),
                                            Text(
                                              value.title.toString(),
                                              style: CustomTextStyle.medium(
                                                12.sp,
                                              ),
                                            ),
                                          ],
                                        ),
                                      );
                                    }).toList(),
                                    onChanged: (Status? value) {
                                      setState(() {
                                        status = value;
                                        machinePutBloc.add(
                                          MachinePutted(
                                            dataVehicleModel: DataVehicleModel(
                                              noPol: state.dataVehicle.noPol,
                                              condition:
                                                  state.dataVehicle.condition,
                                              brand: state.dataVehicle.brand,
                                              url: state.dataVehicle.url,
                                              model: state.dataVehicle.model,
                                              variant:
                                                  state.dataVehicle.variant,
                                              manufacture:
                                                  state.dataVehicle.manufacture,
                                              mileage:
                                                  state.dataVehicle.mileage,
                                              fuelType:
                                                  state.dataVehicle.fuelType,
                                              transmission: state
                                                  .dataVehicle.transmission,
                                              exterior:
                                                  state.dataVehicle.exterior,
                                              price: state.dataVehicle.price,
                                              notes: state.dataVehicle.notes,
                                              seller: state.dataVehicle.seller,
                                              address:
                                                  state.dataVehicle.address,
                                              mobileNumber: state
                                                  .dataVehicle.mobileNumber,
                                              province:
                                                  state.dataVehicle.province,
                                              district:
                                                  state.dataVehicle.district,
                                              subDistrict:
                                                  state.dataVehicle.subDistrict,
                                              status: value!.id.toString(),
                                              category:
                                                  state.dataVehicle.category,
                                              createdAt:
                                                  state.dataVehicle.createdAt,
                                              updatedAt: DateTime.now()
                                                  .millisecondsSinceEpoch,
                                            ),
                                          ),
                                        );
                                      });
                                    },
                                  ),
                                ],
                              ),
                            ),
                            const MachineSubtitle(
                              title: "Specification",
                            ),
                            MachineSpecification(
                              dataVehicle: state.dataVehicle,
                            ),
                            const MachineSubtitle(
                              title: "Seller & location",
                            ),
                            MachineSellerLocation(
                              dataVehicle: state.dataVehicle,
                            ),
                          ],
                        );
                      } else if (state is MachineFailure) {
                        return SizedBox(
                          width: CustomSize.width(context),
                          height: CustomSize.height(context) / 1.4,
                          child: CustomHandling(
                            withError: true,
                            onTap: () => machineGetBloc.add(
                              MachineGetted(
                                noPol: widget.machineArgument.noPol.toString(),
                              ),
                            ),
                          ),
                        );
                      }
                      return const SizedBox.shrink();
                    },
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
