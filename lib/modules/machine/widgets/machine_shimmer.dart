import 'package:lead_management/packages/packages.dart';

class MachineShimmer extends StatelessWidget {
  const MachineShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomShimmer(
                width: CustomSize.width(context) / 2.6,
                height: 16.h,
              ),
              SizedBox(
                height: 6.h,
              ),
              CustomShimmer(
                width: CustomSize.width(context) / 4.8,
                height: 12.h,
              ),
              SizedBox(
                height: 28.h,
              ),
              CustomShimmer(
                width: CustomSize.width(context),
                height: 40.h,
                borderRadius: 8,
              ),
              SizedBox(
                height: 23.h,
              ),
              Divider(
                height: 2,
                color: CustomColorStyle.black.withOpacity(
                  0.2,
                ),
              ),
              SizedBox(
                height: 20.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomShimmer(
                    width: CustomSize.width(context) / 4.6,
                    height: 10.h,
                  ),
                  CustomShimmer(
                    width: 16.w,
                    height: 16.h,
                    withCircle: true,
                  ),
                ],
              ),
            ],
          ),
        ),
        SizedBox(
          height: 10.h,
        ),
        const MachineShimmerCard(),
        const MachineShimmerCard(
          withBackground: false,
        ),
        const MachineShimmerCard(),
        const MachineShimmerCard(
          withBackground: false,
        ),
        const MachineShimmerCard(),
        const MachineShimmerCard(
          withBackground: false,
        ),
        const MachineShimmerCard(),
        const MachineShimmerCard(
          withBackground: false,
        ),
        const MachineShimmerCard(),
        const MachineShimmerCard(
          withBackground: false,
        ),
        const MachineShimmerCard(),
        const MachineShimmerCard(
          withColumn: true,
          withBackground: false,
        ),
        SizedBox(
          height: 23.h,
        ),
        Divider(
          height: 2,
          color: CustomColorStyle.black.withOpacity(
            0.2,
          ),
        ),
        SizedBox(
          height: 20.h,
        ),
        const MachineShimmerCard(),
        const MachineShimmerCard(
          withBackground: false,
        ),
        const MachineShimmerCard(),
        const MachineShimmerCard(
          withBackground: false,
        ),
        const MachineShimmerCard(),
        const MachineShimmerCard(
          withColumn: true,
          withBackground: false,
        ),
      ],
    );
  }
}
