import 'package:lead_management/packages/packages.dart';

class MachineSellerLocation extends StatelessWidget {
  final DataVehicle dataVehicle;

  const MachineSellerLocation({
    super.key,
    required this.dataVehicle,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MachineCard(
          title: "Seller name",
          description: dataVehicle.seller,
        ),
        MachineCard(
          title: "Mobile number",
          description: dataVehicle.mobileNumber,
          withBackground: false,
        ),
        MachineCard(
          title: "Province",
          description: dataVehicle.province,
        ),
        MachineCard(
          title: "District",
          description: dataVehicle.district,
          withBackground: false,
        ),
        MachineCard(
          title: "Sub-district",
          description: dataVehicle.subDistrict,
        ),
        MachineCard(
          title: "Full address",
          description: dataVehicle.address,
          withBackground: false,
          withColumn: true,
        ),
      ],
    );
  }
}
