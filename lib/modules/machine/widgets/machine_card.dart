import 'package:lead_management/packages/packages.dart';

class MachineCard extends StatelessWidget {
  final String title;
  final String description;
  final bool withBackground;
  final bool withColumn;

  const MachineCard({
    super.key,
    required this.title,
    required this.description,
    this.withBackground = true,
    this.withColumn = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(
        16,
      ),
      color: withBackground
          ? CustomColorStyle.gray.withOpacity(
              0.8,
            )
          : CustomColorStyle.transparent,
      child: withColumn
          ? Column(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: CustomTextStyle.medium(
                        12.sp,
                      ),
                    ),
                    SizedBox(
                      height: 8.h,
                    ),
                    Text(
                      description,
                      style: CustomTextStyle.medium(
                        12.sp,
                        color: CustomColorStyle.black.withOpacity(
                          0.6,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  title,
                  style: CustomTextStyle.medium(
                    12.sp,
                  ),
                ),
                Text(
                  description,
                  style: CustomTextStyle.regular(
                    12.sp,
                    color: CustomColorStyle.black.withOpacity(
                      0.6,
                    ),
                  ),
                )
              ],
            ),
    );
  }
}
