import 'package:lead_management/packages/packages.dart';

class MachineShimmerCard extends StatelessWidget {
  final bool withBackground;
  final bool withColumn;

  const MachineShimmerCard({
    super.key,
    this.withBackground = true,
    this.withColumn = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(
        16,
      ),
      color: withBackground
          ? CustomColorStyle.gray.withOpacity(
              0.8,
            )
          : CustomColorStyle.transparent,
      child: withColumn
          ? Column(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomShimmer(
                      width: CustomSize.width(context) / 2.6,
                      height: 15.h,
                    ),
                    SizedBox(
                      height: 8.h,
                    ),
                    CustomShimmer(
                      width: CustomSize.width(context) / 2,
                      height: 15.h,
                    ),
                  ],
                ),
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomShimmer(
                  width: CustomSize.width(context) / 2.6,
                  height: 15.h,
                ),
                CustomShimmer(
                  width: CustomSize.width(context) / 2.6,
                  height: 15.h,
                ),
              ],
            ),
    );
  }
}
