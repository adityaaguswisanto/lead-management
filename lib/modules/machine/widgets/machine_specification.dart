import 'package:lead_management/packages/packages.dart';

class MachineSpecification extends StatelessWidget {
  final DataVehicle dataVehicle;

  const MachineSpecification({
    super.key,
    required this.dataVehicle,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        MachineCard(
          title: "Registration number",
          description: dataVehicle.noPol,
        ),
        MachineCard(
          title: "Condition",
          description: dataVehicle.condition,
          withBackground: false,
        ),
        MachineCard(
          title: "Brand",
          description: dataVehicle.brand,
        ),
        MachineCard(
          title: "Model",
          description: dataVehicle.model,
          withBackground: false,
        ),
        MachineCard(
          title: "Variant",
          description: dataVehicle.variant,
        ),
        MachineCard(
          title: "Manufacture year",
          description: dataVehicle.manufacture,
          withBackground: false,
        ),
        MachineCard(
          title: "Mileage",
          description: dataVehicle.mileage,
        ),
        MachineCard(
          title: "Fuel type",
          description: dataVehicle.fuelType,
          withBackground: false,
        ),
        MachineCard(
          title: "Transmission",
          description: dataVehicle.transmission,
        ),
        MachineCard(
          title: "Exterior color",
          description: dataVehicle.exterior,
          withBackground: false,
        ),
        MachineCard(
          title: "Price",
          description: "Rp ${dataVehicle.price}",
        ),
        MachineCard(
          title: "Notes",
          description: dataVehicle.notes,
          withBackground: false,
          withColumn: true,
        ),
      ],
    );
  }
}
