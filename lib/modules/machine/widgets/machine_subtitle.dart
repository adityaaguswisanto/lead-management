import 'package:lead_management/packages/packages.dart';

class MachineSubtitle extends StatelessWidget {
  final String title;

  const MachineSubtitle({
    super.key,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 12,
        left: 16,
        right: 16,
        bottom: 16,
      ),
      child: Column(
        children: [
          Divider(
            height: 2,
            color: CustomColorStyle.black.withOpacity(
              0.2,
            ),
          ),
          SizedBox(
            height: 16.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: CustomTextStyle.medium(
                  12.sp,
                  color: CustomColorStyle.green,
                ),
              ),
              Icon(
                Icons.more_vert,
                color: CustomColorStyle.black.withOpacity(
                  0.6,
                ),
                size: 20,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
