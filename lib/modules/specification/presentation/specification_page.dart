import 'package:lead_management/packages/packages.dart';

class SpecificationPage extends StatefulWidget {
  static const routeName = "/registration/specification";

  final SpecificationArgument specificationArgument;

  const SpecificationPage({
    super.key,
    required this.specificationArgument,
  });

  @override
  State<SpecificationPage> createState() => _SpecificationPageState();
}

class _SpecificationPageState extends State<SpecificationPage> {
  final price = TextEditingController();
  final mobileNumber = TextEditingController();
  final note = TextEditingController();

  final vehicleBloc = locator<VehicleBloc>();

  int? countNotes = 0;

  bool? loading;

  Subdistrict? subdistrict;
  District? district;
  Province? province;
  Seller? seller;
  Exterior? exterior;
  Transmission? transmission;
  Fuel? fuel;
  Mileage? mileage;
  Manufacture? manufacture;
  Model? model;
  Variant? variant;
  Condition? condition;
  Brand? brand;

  @override
  void dispose() {
    vehicleBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocListener(
          bloc: vehicleBloc,
          listener: (context, state) {
            if (state is VehicleLoading) {
              setState(() {
                loading = true;
              });
            } else if (state is VehiclePostSuccess) {
              setState(() {
                loading = false;
              });
              CustomNavigation.intentWithoutBack(
                context,
                AdvertisePage.routeName,
                arguments: AdvertiseArgument(
                  noPol: widget.specificationArgument.noPol,
                  category: widget.specificationArgument.category,
                ),
              );
            } else if (state is VehicleFailure) {
              CustomToast.failure("Failure send to data");
              setState(() {
                loading = false;
              });
            }
          },
          child: Padding(
            padding: const EdgeInsets.all(
              16,
            ),
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomBack(
                    title: "Registration Number",
                    onPopInvoked: (bool didPop) =>
                        CustomNavigation.intentWithClearAllRoutes(
                      context,
                      RegistrationPage.routeName,
                      arguments: RegistrationArgument(
                        category: widget.specificationArgument.category,
                      ),
                    ),
                    onTap: () => CustomNavigation.intentWithClearAllRoutes(
                      context,
                      RegistrationPage.routeName,
                      arguments: RegistrationArgument(
                        category: widget.specificationArgument.category,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8.h,
                  ),
                  Text(
                    "Specification",
                    style: CustomTextStyle.medium(
                      16.sp,
                    ),
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  Text(
                    "Vehicle details will be submitted on Yodamobi. Please fill in required information",
                    style: CustomTextStyle.medium(
                      14.sp,
                      color: CustomColorStyle.black.withOpacity(
                        0.6,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Divider(
                    height: 2.h,
                    color: CustomColorStyle.black.withOpacity(
                      0.2,
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Text(
                    "Condition*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Condition",
                    value: condition,
                    items: dataCondition.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Condition? value) {
                      setState(() {
                        condition = value;
                      });
                    },
                  ),
                  Text(
                    "Brand*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Brand",
                    value: brand,
                    items: dataBrand.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Brand? value) {
                      setState(() {
                        brand = value;
                      });
                    },
                  ),
                  Text(
                    "Model*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Model",
                    value: model,
                    items: dataModel.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Model? value) {
                      setState(() {
                        model = value;
                      });
                    },
                  ),
                  Text(
                    "Variant*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Variant",
                    value: variant,
                    items: dataVariant.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Variant? value) {
                      setState(() {
                        variant = value;
                      });
                    },
                  ),
                  Text(
                    "Manufacture year*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Manufacture",
                    value: manufacture,
                    items: dataManufacture.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Manufacture? value) {
                      setState(() {
                        manufacture = value;
                      });
                    },
                  ),
                  Text(
                    "Mileage*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Mileage",
                    value: mileage,
                    items: dataMileage.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Mileage? value) {
                      setState(() {
                        mileage = value;
                      });
                    },
                  ),
                  Text(
                    "Fuel type*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Fuel type",
                    value: fuel,
                    items: dataFuelType.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Fuel? value) {
                      setState(() {
                        fuel = value;
                      });
                    },
                  ),
                  Text(
                    "Transmission*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Transmission",
                    value: transmission,
                    items: dataTransmission.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Transmission? value) {
                      setState(() {
                        transmission = value;
                      });
                    },
                  ),
                  Text(
                    "Exterior color*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Exterior Color",
                    value: exterior,
                    items: dataExterior.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Exterior? value) {
                      setState(() {
                        exterior = value;
                      });
                    },
                  ),
                  Text(
                    "Price*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  Container(
                    width: CustomSize.width(context),
                    margin: const EdgeInsets.only(
                      top: 8,
                      bottom: 16,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        8,
                      ),
                      border: Border.all(
                        color: CustomColorStyle.black.withOpacity(
                          0.2,
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(
                            16,
                          ),
                          child: Text(
                            "Rp",
                            style: CustomTextStyle.medium(
                              12.sp,
                            ),
                          ),
                        ),
                        Container(
                          width: 1.6,
                          height: CustomSize.height(context) / 17,
                          color: CustomColorStyle.black.withOpacity(
                            0.1,
                          ),
                        ),
                        Expanded(
                          child: TextFormField(
                            controller: price,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              CustomIdrFormatter(),
                            ],
                            decoration: CustomFieldStyle.regularWithoutBorder(
                              "Price",
                            ),
                            style: CustomTextStyle.medium(
                              12.sp,
                              color: CustomColorStyle.black,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    "Notes",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  Text(
                    "Write a short additional informations.",
                    style: CustomTextStyle.regular(
                      12.sp,
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  TextFormField(
                    controller: note,
                    keyboardType: TextInputType.multiline,
                    maxLines: 4,
                    decoration: CustomFieldStyle.regular(
                      "Notes",
                    ),
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                    onChanged: (value) {
                      setState(() {
                        countNotes = value.length;
                      });
                    },
                  ),
                  SizedBox(
                    height: 4.h,
                  ),
                  Text(
                    "$countNotes characters left",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Text(
                    "Seller Info*",
                    style: CustomTextStyle.medium(
                      16.sp,
                    ),
                  ),
                  Text(
                    "Please provide seller details",
                    style: CustomTextStyle.medium(
                      12.sp,
                      color: CustomColorStyle.black.withOpacity(
                        0.6,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Divider(
                    height: 2.h,
                    color: CustomColorStyle.black.withOpacity(
                      0.2,
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Text(
                    "Seller*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Seller",
                    value: seller,
                    isExpanded: true,
                    items: dataSeller.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Row(
                          children: [
                            Text(
                              value.title.toString(),
                              style: CustomTextStyle.medium(
                                12.sp,
                              ),
                            ),
                            SizedBox(
                              width: 8.w,
                            ),
                            Expanded(
                              child: Text(
                                value.description.toString(),
                                style: CustomTextStyle.medium(
                                  12.sp,
                                  color: CustomColorStyle.black.withOpacity(
                                    0.6,
                                  ),
                                ),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            ),
                          ],
                        ),
                      );
                    }).toList(),
                    onChanged: (Seller? value) {
                      setState(() {
                        seller = value;
                      });
                    },
                  ),
                  Text(
                    "Mobile number",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  Container(
                    width: CustomSize.width(context),
                    margin: const EdgeInsets.only(
                      top: 8,
                      bottom: 16,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        8,
                      ),
                      border: Border.all(
                        color: CustomColorStyle.black.withOpacity(
                          0.2,
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                            left: 16,
                            top: 16,
                            bottom: 16,
                          ),
                          child: Image.asset(
                            "assets/images/specification/whatsapp.png",
                          ),
                        ),
                        Expanded(
                          child: TextFormField(
                            controller: mobileNumber,
                            keyboardType: TextInputType.number,
                            decoration: CustomFieldStyle.regularWithoutBorder(
                              "Mobile number",
                            ),
                            style: CustomTextStyle.medium(
                              12.sp,
                              color: CustomColorStyle.black,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                  Text(
                    "Vehicle location*",
                    style: CustomTextStyle.medium(
                      16.sp,
                    ),
                  ),
                  Text(
                    "Please provide seller details",
                    style: CustomTextStyle.medium(
                      12.sp,
                      color: CustomColorStyle.black.withOpacity(
                        0.6,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Divider(
                    height: 2.h,
                    color: CustomColorStyle.black.withOpacity(
                      0.2,
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Text(
                    "Province*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Province",
                    value: province,
                    items: dataProvince.map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Province? value) {
                      setState(() {
                        province = value;
                        district = null;
                        subdistrict = null;
                      });
                    },
                  ),
                  Text(
                    "District*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "District",
                    value: district,
                    items: (dataDistrict
                            .where((district) =>
                                district.description ==
                                province?.title.toString())
                            .toList())
                        .map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (District? value) {
                      setState(() {
                        district = value;
                        subdistrict = null;
                      });
                    },
                  ),
                  Text(
                    "Sub-district*",
                    style: CustomTextStyle.medium(
                      12.sp,
                    ),
                  ),
                  CustomDropdown(
                    label: "Sub district",
                    value: subdistrict,
                    items: (dataSubdistrict
                            .where((subdistrict) =>
                                subdistrict.description ==
                                district?.title.toString())
                            .toList())
                        .map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value.title.toString(),
                          style: CustomTextStyle.medium(
                            12.sp,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (Subdistrict? value) {
                      setState(() {
                        subdistrict = value;
                      });
                    },
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  ListView.builder(
                    shrinkWrap: true,
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                    ),
                    physics: const BouncingScrollPhysics(),
                    itemCount: dataRegistration.length,
                    itemBuilder: (context, index) {
                      return SpecificationItem(
                        registration: dataRegistration[index],
                      );
                    },
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Divider(
                    height: 2.h,
                    color: CustomColorStyle.black.withOpacity(
                      0.2,
                    ),
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                  Row(
                    children: [
                      const Spacer(),
                      CustomButton(
                        onPressed: () => CustomNavigation.back(context),
                        label: "Cancel",
                        withOutline: true,
                      ),
                      SizedBox(
                        width: 16.w,
                      ),
                      CustomButton(
                        onPressed: loading == true
                            ? null
                            : () {
                                if (condition == null) {
                                  CustomToast.regular("Condition is not empty");
                                } else if (brand == null) {
                                  CustomToast.regular("Brand is not empty");
                                } else if (model == null) {
                                  CustomToast.regular("Model is not empty");
                                } else if (variant == null) {
                                  CustomToast.regular("Model is not empty");
                                } else if (manufacture == null) {
                                  CustomToast.regular(
                                      "Manufacture is not empty");
                                } else if (mileage == null) {
                                  CustomToast.regular("Mileage is not empty");
                                } else if (fuel == null) {
                                  CustomToast.regular("Fuel Type is not empty");
                                } else if (transmission == null) {
                                  CustomToast.regular(
                                      "Transmission Type is not empty");
                                } else if (exterior == null) {
                                  CustomToast.regular(
                                      "Exterior Color Type is not empty");
                                } else if (price.text == "") {
                                  CustomToast.regular(
                                      "Price Type is not empty");
                                } else if (seller == null) {
                                  CustomToast.regular("Seller is not empty");
                                } else if (mobileNumber.text == "") {
                                  CustomToast.regular(
                                      "Mobile Number is not empty");
                                } else if (province == null) {
                                  CustomToast.regular("Province is not empty");
                                } else if (district == null) {
                                  CustomToast.regular("District is not empty");
                                } else if (subdistrict == null) {
                                  CustomToast.regular(
                                      "Sub District is not empty");
                                } else {
                                  vehicleBloc.add(
                                    VehicleSubmitted(
                                      noPol: widget.specificationArgument.noPol
                                          .toString(),
                                      condition: condition!.title,
                                      brand: brand!.title,
                                      url: brand!.url,
                                      model: model!.title,
                                      variant: variant!.title,
                                      manufacture: manufacture!.title,
                                      mileage: mileage!.title,
                                      fuelType: fuel!.title,
                                      transmission: transmission!.title,
                                      exterior: exterior!.title,
                                      price: price.text,
                                      notes: note.text,
                                      seller: seller!.title,
                                      address: seller!.description,
                                      mobileNumber: mobileNumber.text,
                                      province: province!.title,
                                      district: district!.title,
                                      subDistrict: subdistrict!.title,
                                      status: "1",
                                      category: widget
                                          .specificationArgument.category
                                          .toString(),
                                      createdAt:
                                          DateTime.now().millisecondsSinceEpoch,
                                      updatedAt:
                                          DateTime.now().millisecondsSinceEpoch,
                                    ),
                                  );
                                }
                              },
                        label: "Next",
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
