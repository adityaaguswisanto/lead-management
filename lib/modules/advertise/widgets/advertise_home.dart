import 'package:lead_management/packages/packages.dart';

class AdvertiseHome extends StatelessWidget {
  const AdvertiseHome({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => CustomNavigation.intentWithClearAllRoutes(
        context,
        HomePage.routeName,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            Icons.arrow_back,
            color: CustomColorStyle.black.withOpacity(
              0.6,
            ),
            size: 20,
          ),
          SizedBox(
            width: 8.w,
          ),
          Text(
            "Back to home",
            style: CustomTextStyle.medium(
              12.sp,
              color: CustomColorStyle.black.withOpacity(
                0.8,
              ),
            ),
          )
        ],
      ),
    );
  }
}
