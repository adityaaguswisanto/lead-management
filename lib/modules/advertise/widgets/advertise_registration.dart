import 'package:lead_management/packages/packages.dart';

class AdvertiseRegistration extends StatelessWidget {
  final String category;

  const AdvertiseRegistration({
    super.key,
    required this.category,
  });

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: "Want to submit another? ",
            style: CustomTextStyle.medium(
              12.sp,
              color: CustomColorStyle.black.withOpacity(
                0.6,
              ),
            ),
          ),
          TextSpan(
            text: "Click here",
            style: CustomTextStyle.medium(
              12.sp,
              color: CustomColorStyle.green,
            ),
            recognizer: TapGestureRecognizer()
              ..onTap = () => CustomNavigation.intentWithData(
                    context,
                    RegistrationPage.routeName,
                    RegistrationArgument(
                      category: category,
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
