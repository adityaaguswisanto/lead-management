import 'package:lead_management/packages/packages.dart';

class AdvertisePage extends StatefulWidget {
  static const routeName = "/registration/specification/advertise";

  final AdvertiseArgument advertiseArgument;

  const AdvertisePage({
    super.key,
    required this.advertiseArgument,
  });

  @override
  State<AdvertisePage> createState() => _AdvertisePageState();
}

class _AdvertisePageState extends State<AdvertisePage> {
  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      onPopInvoked: (bool didPop) => CustomNavigation.intentWithClearAllRoutes(
        context,
        HomePage.routeName,
      ),
      child: Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(
              16,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: CustomInfo(
                    width: 60.w,
                    height: 60.h,
                    borderColor: CustomColorStyle.green.withOpacity(
                      0.08,
                    ),
                    backgroundColor: CustomColorStyle.green.withOpacity(0.2),
                    iconColor: CustomColorStyle.green,
                  ),
                ),
                SizedBox(
                  height: 16.h,
                ),
                Text(
                  "Lead has been successfully advertised",
                  style: CustomTextStyle.bold(
                    20.sp,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 10.h,
                ),
                Text(
                  "Check your leads on the Home menu. If a problem occurs, please contact Customer Services.",
                  style: CustomTextStyle.regular(
                    14.sp,
                    color: CustomColorStyle.black.withOpacity(
                      0.6,
                    ),
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 16.h,
                ),
                SizedBox(
                  width: CustomSize.width(context),
                  child: CustomButton(
                    onPressed: () => {
                      CustomNavigation.intentWithoutBack(
                        context,
                        MachinePage.routeName,
                        arguments: MachineArgument(
                          noPol: widget.advertiseArgument.noPol,
                          category: widget.advertiseArgument.category,
                          id: 1,
                        ),
                      ),
                    },
                    label: "Check card",
                  ),
                ),
                SizedBox(
                  height: 16.h,
                ),
                AdvertiseRegistration(
                  category: widget.advertiseArgument.category.toString(),
                ),
                SizedBox(
                  height: 20.h,
                ),
                const AdvertiseHome(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
