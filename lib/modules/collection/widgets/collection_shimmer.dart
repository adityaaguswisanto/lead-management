import 'package:lead_management/packages/packages.dart';

class CollectionShimmer extends StatelessWidget {
  const CollectionShimmer({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      padding: const EdgeInsets.symmetric(
        vertical: 6,
      ),
      physics: const BouncingScrollPhysics(),
      itemCount: 20,
      itemBuilder: (context, index) {
        return Container(
          width: CustomSize.width(context),
          margin: const EdgeInsets.only(
            bottom: 16,
          ),
          padding: const EdgeInsets.symmetric(
            vertical: 8,
            horizontal: 16,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(
              8,
            ),
            border: Border.all(
              color: CustomColorStyle.black.withOpacity(
                0.2,
              ),
            ),
          ),
          child: Row(
            children: [
              CustomShimmer(
                width: 30.w,
                height: 30.h,
                withCircle: true,
              ),
              SizedBox(
                width: 12.w,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomShimmer(
                      width: CustomSize.width(context) / 4,
                      height: 12.h,
                    ),
                    SizedBox(
                      height: 2.w,
                    ),
                    CustomShimmer(
                      width: CustomSize.width(context) / 1.8,
                      height: 12.h,
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
