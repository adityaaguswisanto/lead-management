import 'package:lead_management/packages/packages.dart';

class CollectionPage extends StatefulWidget {
  static const routeName = "/collection";

  const CollectionPage({super.key});

  @override
  State<CollectionPage> createState() => _CollectionPageState();
}

class _CollectionPageState extends State<CollectionPage> {
  final noPol = TextEditingController();

  final collectionBloc = locator<CollectionBloc>();

  @override
  void dispose() {
    collectionBloc.close();
    super.dispose();
  }

  @override
  void initState() {
    collectionBloc.add(
      const CollectionGetted(
        noPol: "",
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(
            16,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomBack(
                title: "Home",
                onPopInvoked: (bool didPop) =>
                    CustomNavigation.intentWithClearAllRoutes(
                  context,
                  HomePage.routeName,
                ),
                onTap: () => CustomNavigation.intentWithClearAllRoutes(
                  context,
                  HomePage.routeName,
                ),
              ),
              SizedBox(
                height: 8.h,
              ),
              Text(
                "Search vehicle",
                style: CustomTextStyle.medium(
                  16.sp,
                ),
              ),
              SizedBox(
                height: 4.h,
              ),
              Text(
                "Please search vehicle registration with number.",
                style: CustomTextStyle.medium(
                  14.sp,
                  color: CustomColorStyle.black.withOpacity(
                    0.6,
                  ),
                ),
              ),
              SizedBox(
                height: 16.h,
              ),
              TextFormField(
                controller: noPol,
                textCapitalization: TextCapitalization.characters,
                inputFormatters: [
                  CustomNopolFormatter(),
                ],
                decoration: CustomFieldStyle.regular(
                  "Search in here",
                ),
                style: CustomTextStyle.medium(
                  12.sp,
                ),
                onChanged: (value) {
                  collectionBloc.add(
                    CollectionGetted(
                      noPol: value.toString(),
                    ),
                  );
                },
              ),
              SizedBox(
                height: 8.h,
              ),
              Row(
                children: [
                  Icon(
                    Icons.info,
                    color: CustomColorStyle.black.withOpacity(
                      0.4,
                    ),
                    size: 14,
                  ),
                  SizedBox(
                    width: 4.h,
                  ),
                  Text(
                    "For example, B 1234 FIX",
                    style: CustomTextStyle.medium(
                      10.sp,
                      color: CustomColorStyle.black.withOpacity(
                        0.6,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 8.h,
              ),
              Expanded(
                child: BlocBuilder(
                  bloc: collectionBloc,
                  builder: (context, state) {
                    if (state is CollectionLoading) {
                      return const CollectionShimmer();
                    } else if (state is CollectionGetSuccess) {
                      if (state.vehicle.dataVehicle.isEmpty) {
                        return CustomHandling(
                          onTap: () => collectionBloc.add(
                            CollectionGetted(
                              noPol: noPol.text,
                            ),
                          ),
                        );
                      }
                      return ListView.builder(
                        shrinkWrap: true,
                        physics: const BouncingScrollPhysics(),
                        padding: const EdgeInsets.symmetric(
                          vertical: 8,
                        ),
                        itemCount: state.vehicle.dataVehicle.length,
                        itemBuilder: (context, index) {
                          return VehicleItem(
                            dataVehicle: state.vehicle.dataVehicle[index],
                            category: state.vehicle.dataVehicle[index].category,
                            idSelected: int.parse(
                              state.vehicle.dataVehicle[index].status,
                            ),
                          );
                        },
                      );
                    } else if (state is VehicleFailure) {
                      return CustomHandling(
                        withError: true,
                        onTap: () => collectionBloc.add(
                          CollectionGetted(
                            noPol: noPol.text,
                          ),
                        ),
                      );
                    }
                    return const SizedBox.shrink();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
