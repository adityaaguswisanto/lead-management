import 'package:lead_management/packages/packages.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case HomePage.routeName:
        return MaterialPageRoute(builder: (_) => const HomePage());
      case CollectionPage.routeName:
        return MaterialPageRoute(builder: (_) => const CollectionPage());
      case AdvertisePage.routeName:
        if (args is AdvertiseArgument) {
          return MaterialPageRoute(
            builder: (_) => AdvertisePage(
              advertiseArgument: args,
            ),
          );
        }
        return notFoundPage();
      case RegistrationPage.routeName:
        if (args is RegistrationArgument) {
          return MaterialPageRoute(
            builder: (_) => RegistrationPage(
              registrationArgument: args,
            ),
          );
        }
        return notFoundPage();
      case MachinePage.routeName:
        if (args is MachineArgument) {
          return MaterialPageRoute(
            builder: (_) => MachinePage(
              machineArgument: args,
            ),
          );
        }
        return notFoundPage();
      case SpecificationPage.routeName:
        if (args is SpecificationArgument) {
          return MaterialPageRoute(
            builder: (_) => SpecificationPage(
              specificationArgument: args,
            ),
          );
        }
        return notFoundPage();
      case VehiclePage.routeName:
        if (args is VehicleArgument) {
          return MaterialPageRoute(
            builder: (_) => VehiclePage(
              vehicleArgument: args,
            ),
          );
        }
        return notFoundPage();
      default:
        return notFoundPage();
    }
  }

  static Route<dynamic> notFoundPage() {
    return MaterialPageRoute(
      builder: (context) {
        return const Scaffold(
          body: Center(
            child: Text(
              "Page Not found",
            ),
          ),
        );
      },
    );
  }
}
