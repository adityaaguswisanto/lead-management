import 'package:lead_management/packages/packages.dart';

void main() {
  init();

  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    CustomPortrait().disablePortrait();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => locator<FinancingBloc>(),
        ),
        BlocProvider(
          create: (_) => locator<VehicleBloc>(),
        ),
        BlocProvider(
          create: (_) => locator<MachineBloc>(),
        ),
        BlocProvider(
          create: (_) => locator<CollectionBloc>(),
        ),
      ],
      child: ScreenUtilInit(
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(
              seedColor: CustomColorStyle.green,
            ),
            useMaterial3: true,
          ),
          home: const HomePage(),
          onGenerateRoute: RouteGenerator.generateRoute,
        ),
      ),
    );
  }
}
