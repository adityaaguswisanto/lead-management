import 'package:lead_management/packages/packages.dart';

@GenerateMocks([
  CollectionRepository,
  CollectionSource,
  GetCurrentCollection,
  FinancingRepository,
  FinancingSource,
  GetCurrentFinancing,
  MachineRepository,
  MachineSource,
  GetCurrentMachine,
  VehicleRepository,
  VehicleSource,
  GetCurrentVehicle,
  CustomSqflite,
])
void main() {}
