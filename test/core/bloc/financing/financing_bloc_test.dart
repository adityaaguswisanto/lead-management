import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';
import '../../../shared/common/common.mocks.dart';

void main() {
  late MockGetCurrentFinancing mockGetCurrentFinancing;
  late FinancingBloc financingBloc;

  setUp(() {
    mockGetCurrentFinancing = MockGetCurrentFinancing();
    financingBloc = FinancingBloc(
      mockGetCurrentFinancing,
    );
  });

  const tFinancing = Financing(
    listing: 89,
    inspecting: 90,
    visited: 60,
    assigningSurveyor: 54,
    surveying: 23,
    approval: 213021,
    purchasingOrder: 3932,
    rejected: 43,
    unitNotAvailable: 84,
  );

  const category = "financing";

  test(
    "Initial State Should Be Initialized",
    () {
      expect(
        financingBloc.state,
        FinancingInitialized(),
      );
    },
  );

  blocTest<FinancingBloc, FinancingState>(
    "Should Emit [Loading, Has Data] When Data Is Gotten Successfully",
    build: () {
      when(
        mockGetCurrentFinancing.get(
          category,
        ),
      ).thenAnswer(
        (_) async => const Right(
          tFinancing,
        ),
      );
      return financingBloc;
    },
    act: (bloc) => bloc.add(
      const FinancingGetted(
        category: category,
      ),
    ),
    expect: () => [
      FinancingLoading(),
      const FinancingGetSuccess(
        tFinancing,
      ),
    ],
    verify: (bloc) {
      verify(
        mockGetCurrentFinancing.get(
          category,
        ),
      );
    },
  );

  blocTest<FinancingBloc, FinancingState>(
    "Should Emit [Loading, Error] When Get Data Is Unsuccessful",
    build: () {
      when(
        mockGetCurrentFinancing.get(
          category,
        ),
      ).thenAnswer(
        (_) async => const Left(
          ServerFailure(
            "Another Failure",
          ),
        ),
      );
      return financingBloc;
    },
    act: (bloc) => bloc.add(
      const FinancingGetted(
        category: category,
      ),
    ),
    expect: () => [
      FinancingLoading(),
      const FinancingFailure(
        "Another Failure",
      ),
    ],
    verify: (bloc) {
      verify(
        mockGetCurrentFinancing.get(
          category,
        ),
      );
    },
  );
}
