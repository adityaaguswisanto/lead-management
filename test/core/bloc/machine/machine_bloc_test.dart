import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';
import '../../../shared/common/common.mocks.dart';

void main() {
  late MockGetCurrentMachine mockGetCurrentMachine;
  late MachineBloc machineBloc;

  setUp(() {
    mockGetCurrentMachine = MockGetCurrentMachine();
    machineBloc = MachineBloc(
      mockGetCurrentMachine,
    );
  });

  const tMachineModel = DataVehicleModel(
    noPol: "B 2323 SLI",
    condition: "Baru",
    brand: "Suzuki",
    url: "assets/images/brand/suzuki.png",
    model: "Cold",
    variant: "3.9 FE 71 Solar",
    manufacture: "2014",
    mileage: "25000 - 35000",
    fuelType: "Diesel",
    transmission: "Manual",
    exterior: "Kuning",
    price: "45,555,555",
    notes: "Ini Notes",
    seller: "Sumber Makmur",
    address: "Jl. Magnolia timur 7, bekasi jawa barat",
    mobileNumber: "089667738923",
    province: "Jawa Barat",
    district: "Bandung",
    subDistrict: "Coblong",
    status: "1",
    category: "financing",
    createdAt: 12,
    updatedAt: 12,
  );

  const tMachine = DataVehicle(
    noPol: "B 2323 SLI",
    condition: "Baru",
    brand: "Suzuki",
    url: "assets/images/brand/suzuki.png",
    model: "Cold",
    variant: "3.9 FE 71 Solar",
    manufacture: "2014",
    mileage: "25000 - 35000",
    fuelType: "Diesel",
    transmission: "Manual",
    exterior: "Kuning",
    price: "45,555,555",
    notes: "Ini Notes",
    seller: "Sumber Makmur",
    address: "Jl. Magnolia timur 7, bekasi jawa barat",
    mobileNumber: "089667738923",
    province: "Jawa Barat",
    district: "Bandung",
    subDistrict: "Coblong",
    status: "1",
    category: "financing",
    createdAt: 12,
    updatedAt: 12,
  );

  const noPol = "B 2323 SLI";

  test(
    "Initial State Should Be Initialized",
    () {
      expect(
        machineBloc.state,
        MachineInitialized(),
      );
    },
  );

  group("Machine Successfully", () {
    blocTest<MachineBloc, MachineState>(
      "Should Emit [Loading, Has Data] When Data Is Gotten Successfully",
      build: () {
        when(
          mockGetCurrentMachine.get(
            noPol,
          ),
        ).thenAnswer(
          (_) async => const Right(
            tMachine,
          ),
        );
        return machineBloc;
      },
      act: (bloc) => bloc.add(
        const MachineGetted(
          noPol: noPol,
        ),
      ),
      expect: () => [
        MachineLoading(),
        const MachineGetSuccess(
          tMachine,
        ),
      ],
      verify: (bloc) {
        verify(
          mockGetCurrentMachine.get(
            noPol,
          ),
        );
      },
    );

    blocTest<MachineBloc, MachineState>(
      "Should Emit [Loading, Put] When Data Is Gotten Successfully",
      build: () {
        when(
          mockGetCurrentMachine.put(
            tMachineModel,
          ),
        ).thenAnswer(
          (_) async => const Right(
            tMachine,
          ),
        );
        return machineBloc;
      },
      act: (bloc) => bloc.add(
        const MachinePutted(
          dataVehicleModel: tMachineModel,
        ),
      ),
      expect: () => [
        MachineLoading(),
        const MachinePutSuccess(
          "Success Updated",
        ),
      ],
      verify: (bloc) {
        verify(
          mockGetCurrentMachine.put(
            tMachineModel,
          ),
        );
      },
    );
  });

  group("Machine Unsuccessful", () {
    blocTest<MachineBloc, MachineState>(
      "Should Emit [Loading, Error] When Get Data Is Unsuccessful",
      build: () {
        when(
          mockGetCurrentMachine.get(
            noPol,
          ),
        ).thenAnswer(
          (_) async => const Left(
            ServerFailure(
              "Another Failure",
            ),
          ),
        );
        return machineBloc;
      },
      act: (bloc) => bloc.add(
        const MachineGetted(
          noPol: noPol,
        ),
      ),
      expect: () => [
        MachineLoading(),
        const MachineFailure(
          "Another Failure",
        ),
      ],
      verify: (bloc) {
        verify(
          mockGetCurrentMachine.get(
            noPol,
          ),
        );
      },
    );

    blocTest<MachineBloc, MachineState>(
      "Should Emit [Loading, Put] When Data Is Gotten Unsuccessful",
      build: () {
        when(
          mockGetCurrentMachine.put(
            tMachineModel,
          ),
        ).thenAnswer(
          (_) async => const Left(
            ServerFailure(
              "Another Failure",
            ),
          ),
        );
        return machineBloc;
      },
      act: (bloc) => bloc.add(
        const MachinePutted(
          dataVehicleModel: tMachineModel,
        ),
      ),
      expect: () => [
        MachineLoading(),
        const MachineFailure(
          "Another Failure",
        ),
      ],
      verify: (bloc) {
        verify(
          mockGetCurrentMachine.put(
            tMachineModel,
          ),
        );
      },
    );
  });
}
