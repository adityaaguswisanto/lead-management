import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';
import '../../../shared/common/common.mocks.dart';

void main() {
  late MockGetCurrentVehicle mockGetCurrentVehicle;
  late VehicleBloc vehicleBloc;

  setUp(() {
    mockGetCurrentVehicle = MockGetCurrentVehicle();
    vehicleBloc = VehicleBloc(
      mockGetCurrentVehicle,
    );
  });

  const tVehicle = Vehicle(
    dataVehicle: [
      DataVehicle(
        noPol: "B 2323 SLI",
        condition: "Baru",
        brand: "Suzuki",
        url: "assets/images/brand/suzuki.png",
        model: "Cold",
        variant: "3.9 FE 71 Solar",
        manufacture: "2014",
        mileage: "25000 - 35000",
        fuelType: "Diesel",
        transmission: "Manual",
        exterior: "Kuning",
        price: "45,555,555",
        notes: "Ini Notes",
        seller: "Sumber Makmur",
        address: "Jl. Magnolia timur 7, bekasi jawa barat",
        mobileNumber: "089667738923",
        province: "Jawa Barat",
        district: "Bandung",
        subDistrict: "Coblong",
        status: "1",
        category: "financing",
        createdAt: 12,
        updatedAt: 12,
      ),
    ],
  );

  const tDataVehicle = DataVehicle(
    noPol: "B 2323 SLI",
    condition: "Baru",
    brand: "Suzuki",
    url: "assets/images/brand/suzuki.png",
    model: "Cold",
    variant: "3.9 FE 71 Solar",
    manufacture: "2014",
    mileage: "25000 - 35000",
    fuelType: "Diesel",
    transmission: "Manual",
    exterior: "Kuning",
    price: "45,555,555",
    notes: "Ini Notes",
    seller: "Sumber Makmur",
    address: "Jl. Magnolia timur 7, bekasi jawa barat",
    mobileNumber: "089667738923",
    province: "Jawa Barat",
    district: "Bandung",
    subDistrict: "Coblong",
    status: "1",
    category: "financing",
    createdAt: 12,
    updatedAt: 12,
  );

  const status = "1";
  const category = "financing";
  const offset = 0;
  const limit = 10;

  test(
    "Initial State Should Be Initialized",
    () {
      expect(
        vehicleBloc.state,
        VehicleInitialized(),
      );
    },
  );

  group("Vehicle Successfully", () {
    blocTest<VehicleBloc, VehicleState>(
      "Should Emit [Loading, Has Data] When Data Is Gotten Successfully",
      build: () {
        when(
          mockGetCurrentVehicle.get(
            status,
            category,
            offset,
            limit,
          ),
        ).thenAnswer(
          (_) async => const Right(
            tVehicle,
          ),
        );
        return vehicleBloc;
      },
      act: (bloc) => bloc.add(
        const VehicleGetted(
          status: status,
          category: category,
          offset: offset,
          limit: limit,
        ),
      ),
      expect: () => [
        VehicleLoading(),
        const VehicleGetSuccess(
          tVehicle,
        ),
      ],
      verify: (bloc) {
        verify(
          mockGetCurrentVehicle.get(
            status,
            category,
            offset,
            limit,
          ),
        );
      },
    );

    blocTest<VehicleBloc, VehicleState>(
      "Should Emit [Loading, Post] When Data Is Gotten Successfully",
      build: () {
        when(
          mockGetCurrentVehicle.post(
            tDataVehicle.noPol,
            tDataVehicle.condition,
            tDataVehicle.brand,
            tDataVehicle.url,
            tDataVehicle.model,
            tDataVehicle.variant,
            tDataVehicle.manufacture,
            tDataVehicle.mileage,
            tDataVehicle.fuelType,
            tDataVehicle.transmission,
            tDataVehicle.exterior,
            tDataVehicle.price,
            tDataVehicle.notes,
            tDataVehicle.seller,
            tDataVehicle.address,
            tDataVehicle.mobileNumber,
            tDataVehicle.province,
            tDataVehicle.district,
            tDataVehicle.subDistrict,
            tDataVehicle.status,
            tDataVehicle.category,
            tDataVehicle.createdAt,
            tDataVehicle.updatedAt,
          ),
        ).thenAnswer(
          (_) async => const Right(
            tDataVehicle,
          ),
        );
        return vehicleBloc;
      },
      act: (bloc) => bloc.add(
        VehicleSubmitted(
          noPol: tDataVehicle.noPol,
          condition: tDataVehicle.condition,
          brand: tDataVehicle.brand,
          url: tDataVehicle.url,
          model: tDataVehicle.model,
          variant: tDataVehicle.variant,
          manufacture: tDataVehicle.manufacture,
          mileage: tDataVehicle.mileage,
          fuelType: tDataVehicle.fuelType,
          transmission: tDataVehicle.transmission,
          exterior: tDataVehicle.exterior,
          price: tDataVehicle.price,
          notes: tDataVehicle.notes,
          seller: tDataVehicle.seller,
          address: tDataVehicle.address,
          mobileNumber: tDataVehicle.mobileNumber,
          province: tDataVehicle.province,
          district: tDataVehicle.district,
          subDistrict: tDataVehicle.subDistrict,
          status: tDataVehicle.status,
          category: tDataVehicle.category,
          createdAt: tDataVehicle.createdAt,
          updatedAt: tDataVehicle.updatedAt,
        ),
      ),
      expect: () =>
          [VehicleLoading(), const VehiclePostSuccess("Success send to data")],
      verify: (bloc) {
        verify(
          mockGetCurrentVehicle.post(
            tDataVehicle.noPol,
            tDataVehicle.condition,
            tDataVehicle.brand,
            tDataVehicle.url,
            tDataVehicle.model,
            tDataVehicle.variant,
            tDataVehicle.manufacture,
            tDataVehicle.mileage,
            tDataVehicle.fuelType,
            tDataVehicle.transmission,
            tDataVehicle.exterior,
            tDataVehicle.price,
            tDataVehicle.notes,
            tDataVehicle.seller,
            tDataVehicle.address,
            tDataVehicle.mobileNumber,
            tDataVehicle.province,
            tDataVehicle.district,
            tDataVehicle.subDistrict,
            tDataVehicle.status,
            tDataVehicle.category,
            tDataVehicle.createdAt,
            tDataVehicle.updatedAt,
          ),
        );
      },
    );
  });

  group("Vehicle Unsuccessful", () {
    blocTest<VehicleBloc, VehicleState>(
      "Should Emit [Loading, Error] When Get Data Is Unsuccessful",
      build: () {
        when(
          mockGetCurrentVehicle.get(
            status,
            category,
            offset,
            limit,
          ),
        ).thenAnswer(
          (_) async => const Left(
            ServerFailure(
              "Another Failure",
            ),
          ),
        );
        return vehicleBloc;
      },
      act: (bloc) => bloc.add(
        const VehicleGetted(
          status: status,
          category: category,
          offset: offset,
          limit: limit,
        ),
      ),
      expect: () => [
        VehicleLoading(),
        const VehicleFailure(
          "Another Failure",
        ),
      ],
      verify: (bloc) {
        verify(
          mockGetCurrentVehicle.get(
            status,
            category,
            offset,
            limit,
          ),
        );
      },
    );

    blocTest<VehicleBloc, VehicleState>(
      "Should Emit [Loading, Post] When Data Is Gotten Unsuccessful",
      build: () {
        when(
          mockGetCurrentVehicle.post(
            tDataVehicle.noPol,
            tDataVehicle.condition,
            tDataVehicle.brand,
            tDataVehicle.url,
            tDataVehicle.model,
            tDataVehicle.variant,
            tDataVehicle.manufacture,
            tDataVehicle.mileage,
            tDataVehicle.fuelType,
            tDataVehicle.transmission,
            tDataVehicle.exterior,
            tDataVehicle.price,
            tDataVehicle.notes,
            tDataVehicle.seller,
            tDataVehicle.address,
            tDataVehicle.mobileNumber,
            tDataVehicle.province,
            tDataVehicle.district,
            tDataVehicle.subDistrict,
            tDataVehicle.status,
            tDataVehicle.category,
            tDataVehicle.createdAt,
            tDataVehicle.updatedAt,
          ),
        ).thenAnswer(
          (_) async => const Left(
            ServerFailure(
              "Another Failure",
            ),
          ),
        );
        return vehicleBloc;
      },
      act: (bloc) => bloc.add(
        VehicleSubmitted(
          noPol: tDataVehicle.noPol,
          condition: tDataVehicle.condition,
          brand: tDataVehicle.brand,
          url: tDataVehicle.url,
          model: tDataVehicle.model,
          variant: tDataVehicle.variant,
          manufacture: tDataVehicle.manufacture,
          mileage: tDataVehicle.mileage,
          fuelType: tDataVehicle.fuelType,
          transmission: tDataVehicle.transmission,
          exterior: tDataVehicle.exterior,
          price: tDataVehicle.price,
          notes: tDataVehicle.notes,
          seller: tDataVehicle.seller,
          address: tDataVehicle.address,
          mobileNumber: tDataVehicle.mobileNumber,
          province: tDataVehicle.province,
          district: tDataVehicle.district,
          subDistrict: tDataVehicle.subDistrict,
          status: tDataVehicle.status,
          category: tDataVehicle.category,
          createdAt: tDataVehicle.createdAt,
          updatedAt: tDataVehicle.updatedAt,
        ),
      ),
      expect: () => [
        VehicleLoading(),
        const VehicleFailure(
          "Another Failure",
        ),
      ],
      verify: (bloc) {
        verify(
          mockGetCurrentVehicle.post(
            tDataVehicle.noPol,
            tDataVehicle.condition,
            tDataVehicle.brand,
            tDataVehicle.url,
            tDataVehicle.model,
            tDataVehicle.variant,
            tDataVehicle.manufacture,
            tDataVehicle.mileage,
            tDataVehicle.fuelType,
            tDataVehicle.transmission,
            tDataVehicle.exterior,
            tDataVehicle.price,
            tDataVehicle.notes,
            tDataVehicle.seller,
            tDataVehicle.address,
            tDataVehicle.mobileNumber,
            tDataVehicle.province,
            tDataVehicle.district,
            tDataVehicle.subDistrict,
            tDataVehicle.status,
            tDataVehicle.category,
            tDataVehicle.createdAt,
            tDataVehicle.updatedAt,
          ),
        );
      },
    );
  });
}
