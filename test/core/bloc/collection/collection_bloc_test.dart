import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';
import '../../../shared/common/common.mocks.dart';

void main() {
  late MockGetCurrentCollection mockGetCurrentCollection;
  late CollectionBloc collectionBloc;

  setUp(() {
    mockGetCurrentCollection = MockGetCurrentCollection();
    collectionBloc = CollectionBloc(
      mockGetCurrentCollection,
    );
  });

  const tCollection = Vehicle(
    dataVehicle: [
      DataVehicle(
        noPol: "B 2323 SLI",
        condition: "Baru",
        brand: "Suzuki",
        url: "assets/images/brand/suzuki.png",
        model: "Cold",
        variant: "3.9 FE 71 Solar",
        manufacture: "2014",
        mileage: "25000 - 35000",
        fuelType: "Diesel",
        transmission: "Manual",
        exterior: "Kuning",
        price: "45,555,555",
        notes: "Ini Notes",
        seller: "Sumber Makmur",
        address: "Jl. Magnolia timur 7, bekasi jawa barat",
        mobileNumber: "089667738923",
        province: "Jawa Barat",
        district: "Bandung",
        subDistrict: "Coblong",
        status: "1",
        category: "financing",
        createdAt: 12,
        updatedAt: 12,
      ),
    ],
  );

  const noPol = "B 2323 SLI";

  test(
    "Initial State Should Be Initialized",
    () {
      expect(
        collectionBloc.state,
        CollectionInitialized(),
      );
    },
  );

  blocTest<CollectionBloc, CollectionState>(
    "Should Emit [Loading, Has Data] When Data Is Gotten Successfully",
    build: () {
      when(
        mockGetCurrentCollection.get(
          noPol,
        ),
      ).thenAnswer(
        (_) async => const Right(
          tCollection,
        ),
      );
      return collectionBloc;
    },
    act: (bloc) => bloc.add(
      const CollectionGetted(
        noPol: noPol,
      ),
    ),
    expect: () => [
      CollectionLoading(),
      const CollectionGetSuccess(
        tCollection,
      ),
    ],
    verify: (bloc) {
      verify(
        mockGetCurrentCollection.get(
          noPol,
        ),
      );
    },
  );

  blocTest<CollectionBloc, CollectionState>(
    "Should Emit [Loading, Error] When Get Data Is Unsuccessful",
    build: () {
      when(
        mockGetCurrentCollection.get(
          noPol,
        ),
      ).thenAnswer(
        (_) async => const Left(
          ServerFailure(
            "Another Failure",
          ),
        ),
      );
      return collectionBloc;
    },
    act: (bloc) => bloc.add(
      const CollectionGetted(
        noPol: noPol,
      ),
    ),
    expect: () => [
      CollectionLoading(),
      const CollectionFailure(
        "Another Failure",
      ),
    ],
    verify: (bloc) {
      verify(
        mockGetCurrentCollection.get(
          noPol,
        ),
      );
    },
  );
}
