import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';

import '../../../shared/common/common.mocks.dart';

void main() {
  late MockMachineRepository mockMachineRepository;
  late GetCurrentMachine getCurrentMachine;

  setUp(() {
    mockMachineRepository = MockMachineRepository();
    getCurrentMachine = GetCurrentMachine(
      mockMachineRepository,
    );
  });

  const tDataVehicleModel = DataVehicleModel(
    noPol: "B 2323 SLI",
    condition: "Baru",
    brand: "Suzuki",
    url: "assets/images/brand/suzuki.png",
    model: "Cold",
    variant: "3.9 FE 71 Solar",
    manufacture: "2014",
    mileage: "25000 - 35000",
    fuelType: "Diesel",
    transmission: "Manual",
    exterior: "Kuning",
    price: "45,555,555",
    notes: "Ini Notes",
    seller: "Sumber Makmur",
    address: "Jl. Magnolia timur 7, bekasi jawa barat",
    mobileNumber: "089667738923",
    province: "Jawa Barat",
    district: "Bandung",
    subDistrict: "Coblong",
    status: "1",
    category: "financing",
    createdAt: 12,
    updatedAt: 12,
  );

  const tDataVehicle = DataVehicle(
    noPol: "B 2323 SLI",
    condition: "Baru",
    brand: "Suzuki",
    url: "assets/images/brand/suzuki.png",
    model: "Cold",
    variant: "3.9 FE 71 Solar",
    manufacture: "2014",
    mileage: "25000 - 35000",
    fuelType: "Diesel",
    transmission: "Manual",
    exterior: "Kuning",
    price: "45,555,555",
    notes: "Ini Notes",
    seller: "Sumber Makmur",
    address: "Jl. Magnolia timur 7, bekasi jawa barat",
    mobileNumber: "089667738923",
    province: "Jawa Barat",
    district: "Bandung",
    subDistrict: "Coblong",
    status: "1",
    category: "financing",
    createdAt: 12,
    updatedAt: 12,
  );

  const noPol = "B 2323 SLI";

  group("Should Get Current Machine From The Repository", () {
    test("Get Current Machine", () async {
      //arrage
      when(
        mockMachineRepository.getCurrentMachine(
          noPol,
        ),
      ).thenAnswer(
        (_) async => const Right(
          tDataVehicle,
        ),
      );

      //act
      final result = await getCurrentMachine.get(
        noPol,
      );

      //assert
      expect(
        result,
        equals(
          const Right(
            tDataVehicle,
          ),
        ),
      );
    });

    test("Put Current Machine", () async {
      //arrage
      when(
        mockMachineRepository.putCurrentMachine(
          tDataVehicleModel,
        ),
      ).thenAnswer(
        (_) async => const Right(
          tDataVehicle,
        ),
      );

      //act
      final result = await getCurrentMachine.put(
        tDataVehicleModel,
      );

      //assert
      expect(
        result,
        equals(
          const Right(
            tDataVehicle,
          ),
        ),
      );
    });
  });
}
