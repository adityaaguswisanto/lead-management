import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';

import '../../../shared/common/common.mocks.dart';

void main() {
  late MockFinancingRepository mockFinancingRepository;
  late GetCurrentFinancing getCurrentFinancing;

  setUp(() {
    mockFinancingRepository = MockFinancingRepository();
    getCurrentFinancing = GetCurrentFinancing(
      mockFinancingRepository,
    );
  });

  const tFinancing = Financing(
    listing: 89,
    inspecting: 90,
    visited: 60,
    assigningSurveyor: 54,
    surveying: 23,
    approval: 213021,
    purchasingOrder: 3932,
    rejected: 43,
    unitNotAvailable: 84,
  );

  const category = "financing";

  test("Get Current Financing", () async {
    //arrage
    when(
      mockFinancingRepository.getCurrentFinancing(
        category,
      ),
    ).thenAnswer(
      (_) async => const Right(
        tFinancing,
      ),
    );

    //act
    final result = await getCurrentFinancing.get(
      category,
    );

    //assert
    expect(
      result,
      equals(
        const Right(
          tFinancing,
        ),
      ),
    );
  });
}
