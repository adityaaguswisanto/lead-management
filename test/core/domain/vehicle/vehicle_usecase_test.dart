import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';

import '../../../shared/common/common.mocks.dart';

void main() {
  late MockVehicleRepository mockVehicleRepository;
  late GetCurrentVehicle getCurrentVehicle;

  setUp(() {
    mockVehicleRepository = MockVehicleRepository();
    getCurrentVehicle = GetCurrentVehicle(
      mockVehicleRepository,
    );
  });

  const tVehicle = Vehicle(
    dataVehicle: [
      DataVehicle(
        noPol: "B 2323 SLI",
        condition: "Baru",
        brand: "Suzuki",
        url: "assets/images/brand/suzuki.png",
        model: "Cold",
        variant: "3.9 FE 71 Solar",
        manufacture: "2014",
        mileage: "25000 - 35000",
        fuelType: "Diesel",
        transmission: "Manual",
        exterior: "Kuning",
        price: "45,555,555",
        notes: "Ini Notes",
        seller: "Sumber Makmur",
        address: "Jl. Magnolia timur 7, bekasi jawa barat",
        mobileNumber: "089667738923",
        province: "Jawa Barat",
        district: "Bandung",
        subDistrict: "Coblong",
        status: "1",
        category: "financing",
        createdAt: 12,
        updatedAt: 12,
      ),
    ],
  );

  const tDataVehicle = DataVehicle(
    noPol: "B 2323 SLI",
    condition: "Baru",
    brand: "Suzuki",
    url: "assets/images/brand/suzuki.png",
    model: "Cold",
    variant: "3.9 FE 71 Solar",
    manufacture: "2014",
    mileage: "25000 - 35000",
    fuelType: "Diesel",
    transmission: "Manual",
    exterior: "Kuning",
    price: "45,555,555",
    notes: "Ini Notes",
    seller: "Sumber Makmur",
    address: "Jl. Magnolia timur 7, bekasi jawa barat",
    mobileNumber: "089667738923",
    province: "Jawa Barat",
    district: "Bandung",
    subDistrict: "Coblong",
    status: "1",
    category: "financing",
    createdAt: 12,
    updatedAt: 12,
  );

  const status = "1";
  const category = "financing";
  const offset = 0;
  const limit = 10;

  group("Should Get Current Vehicle From The Repository", () {
    test("Get Current Vehicle", () async {
      //arrage
      when(
        mockVehicleRepository.getCurrentVehicle(
          status,
          category,
          offset,
          limit,
        ),
      ).thenAnswer(
        (_) async => const Right(
          tVehicle,
        ),
      );

      //act
      final result = await getCurrentVehicle.get(
        status,
        category,
        offset,
        limit,
      );

      //assert
      expect(
        result,
        equals(
          const Right(
            tVehicle,
          ),
        ),
      );
    });

    test("Post Current Vehicle", () async {
      //arrage
      when(
        mockVehicleRepository.postCurrentVehicle(
          tDataVehicle.noPol,
          tDataVehicle.condition,
          tDataVehicle.brand,
          tDataVehicle.url,
          tDataVehicle.model,
          tDataVehicle.variant,
          tDataVehicle.manufacture,
          tDataVehicle.mileage,
          tDataVehicle.fuelType,
          tDataVehicle.transmission,
          tDataVehicle.exterior,
          tDataVehicle.price,
          tDataVehicle.notes,
          tDataVehicle.seller,
          tDataVehicle.address,
          tDataVehicle.mobileNumber,
          tDataVehicle.province,
          tDataVehicle.district,
          tDataVehicle.subDistrict,
          tDataVehicle.status,
          tDataVehicle.category,
          tDataVehicle.createdAt,
          tDataVehicle.updatedAt,
        ),
      ).thenAnswer(
        (_) async => const Right(
          tDataVehicle,
        ),
      );

      //act
      final result = await getCurrentVehicle.post(
        tDataVehicle.noPol,
        tDataVehicle.condition,
        tDataVehicle.brand,
        tDataVehicle.url,
        tDataVehicle.model,
        tDataVehicle.variant,
        tDataVehicle.manufacture,
        tDataVehicle.mileage,
        tDataVehicle.fuelType,
        tDataVehicle.transmission,
        tDataVehicle.exterior,
        tDataVehicle.price,
        tDataVehicle.notes,
        tDataVehicle.seller,
        tDataVehicle.address,
        tDataVehicle.mobileNumber,
        tDataVehicle.province,
        tDataVehicle.district,
        tDataVehicle.subDistrict,
        tDataVehicle.status,
        tDataVehicle.category,
        tDataVehicle.createdAt,
        tDataVehicle.updatedAt,
      );

      //assert
      expect(
        result,
        equals(
          const Right(
            tDataVehicle,
          ),
        ),
      );
    });
  });
}
