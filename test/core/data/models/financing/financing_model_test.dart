import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';

import '../../../../shared/helper/helper.dart';

void main() {
  const tFinancingModel = FinancingModel(
    listing: 89,
    inspecting: 90,
    visited: 60,
    assigningSurveyor: 54,
    surveying: 23,
    approval: 213021,
    purchasingOrder: 3932,
    rejected: 43,
    unitNotAvailable: 84,
  );

  const tFinancing = Financing(
    listing: 89,
    inspecting: 90,
    visited: 60,
    assigningSurveyor: 54,
    surveying: 23,
    approval: 213021,
    purchasingOrder: 3932,
    rejected: 43,
    unitNotAvailable: 84,
  );
  test(
    "Should Be A Subclass Of Entity Financing",
    () async {
      // assert
      final result = tFinancingModel.toEntity();

      expect(
        result,
        equals(
          tFinancing,
        ),
      );
    },
  );

  test(
    "Should Return A Valid Model From Json Financing",
    () async {
      // arrange
      final Map<String, dynamic> jsonMap = json.decode(
        await File(
          testPath(
            "shared/data/financing/financing.json",
          ),
        ).readAsString(),
      );

      // act
      final result = FinancingModel.fromJson(
        jsonMap,
      );

      // assert
      expect(
        result,
        equals(
          tFinancingModel,
        ),
      );
    },
  );

  test(
    "Should Return A Json Map Containing Proper Data Financing",
    () async {
      // act
      final result = tFinancingModel.toJson();

      // assert
      final expectedJsonMap = {
        "listing": 89,
        "inspecting": 90,
        "visited": 60,
        "assigning_surveyor": 54,
        "surveying": 23,
        "approval": 213021,
        "purchasing_order": 3932,
        "rejected": 43,
        "unit_not_available": 84
      };

      // assert
      expect(
        result,
        equals(
          expectedJsonMap,
        ),
      );
    },
  );
}
