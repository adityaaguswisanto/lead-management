import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';

import '../../../../shared/helper/helper.dart';

void main() {
  const tVehicleModel = VehicleModel(
    dataVehicle: [
      DataVehicleModel(
        noPol: "B 2323 SLI",
        condition: "Baru",
        brand: "Suzuki",
        url: "assets/images/brand/suzuki.png",
        model: "Cold",
        variant: "3.9 FE 71 Solar",
        manufacture: "2014",
        mileage: "25000 - 35000",
        fuelType: "Diesel",
        transmission: "Manual",
        exterior: "Kuning",
        price: "45,555,555",
        notes: "Ini Notes",
        seller: "Sumber Makmur",
        address: "Jl. Magnolia timur 7, bekasi jawa barat",
        mobileNumber: "089667738923",
        province: "Jawa Barat",
        district: "Bandung",
        subDistrict: "Coblong",
        status: "1",
        category: "financing",
        createdAt: 12,
        updatedAt: 12,
      ),
    ],
  );

  const tVehicle = Vehicle(
    dataVehicle: [
      DataVehicle(
        noPol: "B 2323 SLI",
        condition: "Baru",
        brand: "Suzuki",
        url: "assets/images/brand/suzuki.png",
        model: "Cold",
        variant: "3.9 FE 71 Solar",
        manufacture: "2014",
        mileage: "25000 - 35000",
        fuelType: "Diesel",
        transmission: "Manual",
        exterior: "Kuning",
        price: "45,555,555",
        notes: "Ini Notes",
        seller: "Sumber Makmur",
        address: "Jl. Magnolia timur 7, bekasi jawa barat",
        mobileNumber: "089667738923",
        province: "Jawa Barat",
        district: "Bandung",
        subDistrict: "Coblong",
        status: "1",
        category: "financing",
        createdAt: 12,
        updatedAt: 12,
      ),
    ],
  );

  test(
    "Should Be A Subclass Of Entity Vehicle",
    () async {
      // assert
      final result = tVehicleModel.toEntity();

      expect(
        result,
        equals(
          tVehicle,
        ),
      );
    },
  );

  test(
    "Should Return A Valid Model From Json Vehicle",
    () async {
      // arrange
      final Map<String, dynamic> jsonMap = json.decode(
        await File(
          testPath(
            "shared/data/vehicle/vehicle.json",
          ),
        ).readAsString(),
      );

      // act
      final result = VehicleModel.fromJson(
        jsonMap,
      );

      // assert
      expect(
        result,
        equals(
          tVehicleModel,
        ),
      );
    },
  );

  test(
    "Should Return A Json Map Containing Proper Data Vehicle",
    () async {
      // act
      final result = tVehicleModel.toJson();

      // assert
      final expectedJsonMap = {
        "data_vehicle": [
          const DataVehicleModel(
            noPol: "B 2323 SLI",
            condition: "Baru",
            brand: "Suzuki",
            url: "assets/images/brand/suzuki.png",
            model: "Cold",
            variant: "3.9 FE 71 Solar",
            manufacture: "2014",
            mileage: "25000 - 35000",
            fuelType: "Diesel",
            transmission: "Manual",
            exterior: "Kuning",
            price: "45,555,555",
            notes: "Ini Notes",
            seller: "Sumber Makmur",
            address: "Jl. Magnolia timur 7, bekasi jawa barat",
            mobileNumber: "089667738923",
            province: "Jawa Barat",
            district: "Bandung",
            subDistrict: "Coblong",
            status: "1",
            category: "financing",
            createdAt: 12,
            updatedAt: 12,
          ),
        ]
      };

      // assert
      expect(
        result,
        equals(
          expectedJsonMap,
        ),
      );
    },
  );
}
