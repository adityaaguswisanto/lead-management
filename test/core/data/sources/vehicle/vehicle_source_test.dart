import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';

import '../../../../shared/common/common.mocks.dart';

void main() {
  late MockCustomSqflite mockCustomSqflite;
  late VehicleSourceImpl vehicleSourceImpl;

  setUp(() {
    mockCustomSqflite = MockCustomSqflite();
    vehicleSourceImpl = VehicleSourceImpl(
      customSqflite: mockCustomSqflite,
    );
  });

  const tVehicleModel = VehicleModel(
    dataVehicle: [
      DataVehicleModel(
        noPol: "B 2323 SLI",
        condition: "Baru",
        brand: "Suzuki",
        url: "assets/images/brand/suzuki.png",
        model: "Cold",
        variant: "3.9 FE 71 Solar",
        manufacture: "2014",
        mileage: "25000 - 35000",
        fuelType: "Diesel",
        transmission: "Manual",
        exterior: "Kuning",
        price: "45,555,555",
        notes: "Ini Notes",
        seller: "Sumber Makmur",
        address: "Jl. Magnolia timur 7, bekasi jawa barat",
        mobileNumber: "089667738923",
        province: "Jawa Barat",
        district: "Bandung",
        subDistrict: "Coblong",
        status: "1",
        category: "financing",
        createdAt: 12,
        updatedAt: 12,
      ),
    ],
  );

  const tDataVehicleModel = DataVehicleModel(
    noPol: "B 2323 SLI",
    condition: "Baru",
    brand: "Suzuki",
    url: "assets/images/brand/suzuki.png",
    model: "Cold",
    variant: "3.9 FE 71 Solar",
    manufacture: "2014",
    mileage: "25000 - 35000",
    fuelType: "Diesel",
    transmission: "Manual",
    exterior: "Kuning",
    price: "45,555,555",
    notes: "Ini Notes",
    seller: "Sumber Makmur",
    address: "Jl. Magnolia timur 7, bekasi jawa barat",
    mobileNumber: "089667738923",
    province: "Jawa Barat",
    district: "Bandung",
    subDistrict: "Coblong",
    status: "1",
    category: "financing",
    createdAt: 12,
    updatedAt: 12,
  );

  const status = "1";
  const category = "financing";
  const offset = 0;
  const limit = 10;

  group("Read & Create Return From Sqflite", () {
    test(
      "Read Vehicle",
      () async {
        // arrange
        when(
          mockCustomSqflite.readVehicle(
            status,
            category,
            offset,
            limit,
          ),
        ).thenAnswer(
          (_) async => tVehicleModel,
        );

        // act
        final result = await vehicleSourceImpl.getCurrentVehicle(
          status,
          category,
          offset,
          limit,
        );

        // assert
        verify(
          mockCustomSqflite.readVehicle(
            status,
            category,
            offset,
            limit,
          ),
        );

        expect(
          result,
          tVehicleModel,
        );
      },
    );

    test(
      "Post Vehicle",
      () async {
        // arrange
        when(
          mockCustomSqflite.createVehicle(
            tDataVehicleModel,
          ),
        ).thenAnswer(
          (_) async => tDataVehicleModel,
        );

        // act
        final result = await vehicleSourceImpl.postCurrentVehicle(
          tDataVehicleModel.noPol,
          tDataVehicleModel.condition,
          tDataVehicleModel.brand,
          tDataVehicleModel.url,
          tDataVehicleModel.model,
          tDataVehicleModel.variant,
          tDataVehicleModel.manufacture,
          tDataVehicleModel.mileage,
          tDataVehicleModel.fuelType,
          tDataVehicleModel.transmission,
          tDataVehicleModel.exterior,
          tDataVehicleModel.price,
          tDataVehicleModel.notes,
          tDataVehicleModel.seller,
          tDataVehicleModel.address,
          tDataVehicleModel.mobileNumber,
          tDataVehicleModel.province,
          tDataVehicleModel.district,
          tDataVehicleModel.subDistrict,
          tDataVehicleModel.status,
          tDataVehicleModel.category,
          tDataVehicleModel.createdAt,
          tDataVehicleModel.updatedAt,
        );

        // assert
        verify(
          mockCustomSqflite.createVehicle(
            tDataVehicleModel,
          ),
        );

        expect(
          result,
          tDataVehicleModel,
        );
      },
    );
  });
}
