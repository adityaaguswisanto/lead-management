import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';

import '../../../../shared/common/common.mocks.dart';

void main() {
  late MockCustomSqflite mockCustomSqflite;
  late FinancingSourceImpl financingSourceImpl;

  setUp(() {
    mockCustomSqflite = MockCustomSqflite();
    financingSourceImpl = FinancingSourceImpl(
      customSqflite: mockCustomSqflite,
    );
  });

  const tFinancingModel = FinancingModel(
    listing: 89,
    inspecting: 90,
    visited: 60,
    assigningSurveyor: 54,
    surveying: 23,
    approval: 213021,
    purchasingOrder: 3932,
    rejected: 43,
    unitNotAvailable: 84,
  );

  const category = "financing";

  test(
    "Read Financing Return From Sqflite",
    () async {
      // arrange
      when(
        mockCustomSqflite.readFinancing(
          category,
        ),
      ).thenAnswer(
        (_) async => tFinancingModel,
      );

      // act
      final result = await financingSourceImpl.getCurrentFinancing(
        category,
      );

      // assert
      verify(
        mockCustomSqflite.readFinancing(
          category,
        ),
      );

      expect(
        result,
        tFinancingModel,
      );
    },
  );
}
