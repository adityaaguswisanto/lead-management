import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';

import '../../../../shared/common/common.mocks.dart';

void main() {
  late MockCustomSqflite mockCustomSqflite;
  late CollectionSourceImpl collectionSourceImpl;

  setUp(() {
    mockCustomSqflite = MockCustomSqflite();
    collectionSourceImpl = CollectionSourceImpl(
      customSqflite: mockCustomSqflite,
    );
  });

  const tVehicleModel = VehicleModel(
    dataVehicle: [
      DataVehicleModel(
        noPol: "B 2323 SLI",
        condition: "Baru",
        brand: "Suzuki",
        url: "assets/images/brand/suzuki.png",
        model: "Cold",
        variant: "3.9 FE 71 Solar",
        manufacture: "2014",
        mileage: "25000 - 35000",
        fuelType: "Diesel",
        transmission: "Manual",
        exterior: "Kuning",
        price: "45,555,555",
        notes: "Ini Notes",
        seller: "Sumber Makmur",
        address: "Jl. Magnolia timur 7, bekasi jawa barat",
        mobileNumber: "089667738923",
        province: "Jawa Barat",
        district: "Bandung",
        subDistrict: "Coblong",
        status: "1",
        category: "financing",
        createdAt: 12,
        updatedAt: 12,
      ),
    ],
  );

  const noPol = "B 2323 SLI";

  test(
    "Read Collection Return From Sqflite",
    () async {
      // arrange
      when(
        mockCustomSqflite.readCollection(
          noPol,
        ),
      ).thenAnswer(
        (_) async => tVehicleModel,
      );

      // act
      final result = await collectionSourceImpl.getCurrentCollection(
        noPol,
      );

      // assert
      verify(
        mockCustomSqflite.readCollection(
          noPol,
        ),
      );

      expect(
        result,
        tVehicleModel,
      );
    },
  );
}
