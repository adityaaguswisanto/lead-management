import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';

import '../../../../shared/common/common.mocks.dart';

void main() {
  late MockFinancingSource mockFinancingSource;
  late FinancingRepositoryImpl financingRepositoryImpl;

  setUp(() {
    mockFinancingSource = MockFinancingSource();
    financingRepositoryImpl = FinancingRepositoryImpl(
      financingSource: mockFinancingSource,
    );
  });

  const tFinancingModel = FinancingModel(
    listing: 89,
    inspecting: 90,
    visited: 60,
    assigningSurveyor: 54,
    surveying: 23,
    approval: 213021,
    purchasingOrder: 3932,
    rejected: 43,
    unitNotAvailable: 84,
  );

  const tFinancing = Financing(
    listing: 89,
    inspecting: 90,
    visited: 60,
    assigningSurveyor: 54,
    surveying: 23,
    approval: 213021,
    purchasingOrder: 3932,
    rejected: 43,
    unitNotAvailable: 84,
  );

  const category = "financing";

  group("Get Current Financing", () {
    test(
      "Should Return Current Financing When A Call To Data Source Is Successful",
      () async {
        // arrange
        when(
          mockFinancingSource.getCurrentFinancing(
            category,
          ),
        ).thenAnswer(
          (_) async => tFinancingModel,
        );

        // act
        final result = await financingRepositoryImpl.getCurrentFinancing(
          category,
        );

        // assert
        verify(
          mockFinancingSource.getCurrentFinancing(
            category,
          ),
        );

        expect(
          result,
          equals(
            const Right(
              tFinancing,
            ),
          ),
        );
      },
    );

    test(
      "Should Return Server Failure When A Call To Data Source Is Unsuccessful",
      () async {
        // arrange
        when(
          mockFinancingSource.getCurrentFinancing(
            category,
          ),
        ).thenThrow(
          const ServerException("Failure Getted"),
        );

        // act
        final result = await financingRepositoryImpl.getCurrentFinancing(
          category,
        );

        // assert
        verify(
          mockFinancingSource.getCurrentFinancing(
            category,
          ),
        );

        expect(
          result,
          equals(
            const Left(
              ServerFailure("Failure Getted"),
            ),
          ),
        );
      },
    );
  });
}
