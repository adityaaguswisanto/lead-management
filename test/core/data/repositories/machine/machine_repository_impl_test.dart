import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';

import '../../../../shared/common/common.mocks.dart';

void main() {
  late MockMachineSource mockMachineSource;
  late MachineRepositoryImpl machineRepositoryImpl;

  setUp(() {
    mockMachineSource = MockMachineSource();
    machineRepositoryImpl = MachineRepositoryImpl(
      machineSource: mockMachineSource,
    );
  });

  const tMachineModel = DataVehicleModel(
    noPol: "B 2323 SLI",
    condition: "Baru",
    brand: "Suzuki",
    url: "assets/images/brand/suzuki.png",
    model: "Cold",
    variant: "3.9 FE 71 Solar",
    manufacture: "2014",
    mileage: "25000 - 35000",
    fuelType: "Diesel",
    transmission: "Manual",
    exterior: "Kuning",
    price: "45,555,555",
    notes: "Ini Notes",
    seller: "Sumber Makmur",
    address: "Jl. Magnolia timur 7, bekasi jawa barat",
    mobileNumber: "089667738923",
    province: "Jawa Barat",
    district: "Bandung",
    subDistrict: "Coblong",
    status: "1",
    category: "financing",
    createdAt: 12,
    updatedAt: 12,
  );

  const tMachine = DataVehicle(
    noPol: "B 2323 SLI",
    condition: "Baru",
    brand: "Suzuki",
    url: "assets/images/brand/suzuki.png",
    model: "Cold",
    variant: "3.9 FE 71 Solar",
    manufacture: "2014",
    mileage: "25000 - 35000",
    fuelType: "Diesel",
    transmission: "Manual",
    exterior: "Kuning",
    price: "45,555,555",
    notes: "Ini Notes",
    seller: "Sumber Makmur",
    address: "Jl. Magnolia timur 7, bekasi jawa barat",
    mobileNumber: "089667738923",
    province: "Jawa Barat",
    district: "Bandung",
    subDistrict: "Coblong",
    status: "1",
    category: "financing",
    createdAt: 12,
    updatedAt: 12,
  );

  const noPol = "B 2323 SLI";

  group("Get Current Machine", () {
    test(
      "Should Return Current Machine When A Call To Data Source Is Successful",
      () async {
        // arrange
        when(
          mockMachineSource.getCurrentMachine(
            noPol,
          ),
        ).thenAnswer(
          (_) async => tMachineModel,
        );

        // act
        final result = await machineRepositoryImpl.getCurrentMachine(
          noPol,
        );

        // assert
        verify(
          mockMachineSource.getCurrentMachine(
            noPol,
          ),
        );

        expect(
          result,
          equals(
            const Right(
              tMachine,
            ),
          ),
        );
      },
    );

    test(
      "Should Return Current Machine When A Put To Data Source Is Successful",
      () async {
        // arrange
        when(
          mockMachineSource.putCurrentMachine(
            tMachineModel,
          ),
        ).thenAnswer(
          (_) async => tMachineModel,
        );

        // act
        final result = await machineRepositoryImpl.putCurrentMachine(
          tMachineModel,
        );

        // assert
        verify(
          mockMachineSource.putCurrentMachine(
            tMachineModel,
          ),
        );

        expect(
          result,
          equals(
            const Right(
              tMachine,
            ),
          ),
        );
      },
    );

    test(
      "Should Return Server Failure When A Call To Data Source Is Unsuccessful",
      () async {
        // arrange
        when(
          mockMachineSource.getCurrentMachine(
            noPol,
          ),
        ).thenThrow(
          const ServerException("Failure Getted"),
        );

        // act
        final result = await machineRepositoryImpl.getCurrentMachine(
          noPol,
        );

        // assert
        verify(
          mockMachineSource.getCurrentMachine(
            noPol,
          ),
        );

        expect(
          result,
          equals(
            const Left(
              ServerFailure("Failure Getted"),
            ),
          ),
        );
      },
    );
  });
}
