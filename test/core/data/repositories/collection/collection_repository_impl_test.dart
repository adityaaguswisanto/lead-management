import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:lead_management/packages/packages.dart';

import '../../../../shared/common/common.mocks.dart';

void main() {
  late MockCollectionSource mockCollectionSource;
  late CollectionRepositoryImpl collectionRepositoryImpl;

  setUp(() {
    mockCollectionSource = MockCollectionSource();
    collectionRepositoryImpl = CollectionRepositoryImpl(
      collectionSource: mockCollectionSource,
    );
  });

  const tCollectionModel = VehicleModel(
    dataVehicle: [
      DataVehicleModel(
        noPol: "B 2323 SLI",
        condition: "Baru",
        brand: "Suzuki",
        url: "assets/images/brand/suzuki.png",
        model: "Cold",
        variant: "3.9 FE 71 Solar",
        manufacture: "2014",
        mileage: "25000 - 35000",
        fuelType: "Diesel",
        transmission: "Manual",
        exterior: "Kuning",
        price: "45,555,555",
        notes: "Ini Notes",
        seller: "Sumber Makmur",
        address: "Jl. Magnolia timur 7, bekasi jawa barat",
        mobileNumber: "089667738923",
        province: "Jawa Barat",
        district: "Bandung",
        subDistrict: "Coblong",
        status: "1",
        category: "financing",
        createdAt: 12,
        updatedAt: 12,
      ),
    ],
  );

  const tCollection = Vehicle(
    dataVehicle: [
      DataVehicle(
        noPol: "B 2323 SLI",
        condition: "Baru",
        brand: "Suzuki",
        url: "assets/images/brand/suzuki.png",
        model: "Cold",
        variant: "3.9 FE 71 Solar",
        manufacture: "2014",
        mileage: "25000 - 35000",
        fuelType: "Diesel",
        transmission: "Manual",
        exterior: "Kuning",
        price: "45,555,555",
        notes: "Ini Notes",
        seller: "Sumber Makmur",
        address: "Jl. Magnolia timur 7, bekasi jawa barat",
        mobileNumber: "089667738923",
        province: "Jawa Barat",
        district: "Bandung",
        subDistrict: "Coblong",
        status: "1",
        category: "financing",
        createdAt: 12,
        updatedAt: 12,
      ),
    ],
  );

  const noPol = "B 2323 SLI";

  group("Get Current Collection", () {
    test(
      "Should Return Current Collection When A Call To Data Source Is Successful",
      () async {
        // arrange
        when(
          mockCollectionSource.getCurrentCollection(
            noPol,
          ),
        ).thenAnswer(
          (_) async => tCollectionModel,
        );

        // act
        final result = await collectionRepositoryImpl.getCurrentCollection(
          noPol,
        );

        // assert
        verify(
          mockCollectionSource.getCurrentCollection(
            noPol,
          ),
        );

        expect(
          result,
          equals(
            const Right(
              tCollection,
            ),
          ),
        );
      },
    );

    test(
      "Should Return Server Failure When A Call To Data Source Is Unsuccessful",
      () async {
        // arrange
        when(
          mockCollectionSource.getCurrentCollection(
            noPol,
          ),
        ).thenThrow(
          const ServerException("Failure Getted"),
        );

        // act
        final result = await collectionRepositoryImpl.getCurrentCollection(
          noPol,
        );

        // assert
        verify(
          mockCollectionSource.getCurrentCollection(
            noPol,
          ),
        );

        expect(
          result,
          equals(
            const Left(
              ServerFailure("Failure Getted"),
            ),
          ),
        );
      },
    );
  });
}
